// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _kinton_arm_constr_task_opt_alg_node_h_
#define _kinton_arm_constr_task_opt_alg_node_h_

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "kinton_arm_constr_task_opt_alg.h"

#include <sstream>

#include <urdf/model.h>

#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

// [publisher subscriber headers]
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/JointState.h>
#include <visualization_msgs/MarkerArray.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <std_msgs/Float64MultiArray.h>

// [service client headers]

// [action server client headers]
#include <iri_action_server/iri_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <kinton_msgs/waypointsAction.h>

enum TaskStates{
    IDLE,
    NAVIGATING,
    INWAYPOINT};

/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class KintonArmConstrTaskOptAlgNode : public algorithm_base::IriBaseAlgorithm<KintonArmConstrTaskOptAlgorithm>
{
  private:

    // Time vars
    ros::Time time_,time_ini_; // ROS Time variables.
    double dt_; // Time differential.
    double curr_t_; // Current execution time.
    double time_last_; // Last execution time.
    bool init_; //Initialize timers and arm parameters.

    // Transforms
    Eigen::Matrix4d Tcam_in_tip_; // Homogeneous transform of camera in arm tip frames.
    Eigen::Matrix4d Ttag_in_cam_; // Homogeneous Transform of tag in camera frames.
    Eigen::Matrix4d Tarmbase_in_baselink_; // Homogeneous Transform of arm base in base link.
    Eigen::Matrix4d Tlink1_in_armbase_; // Homogeneous Transform of link 1 in arm base link.

    // Task related
    double vs_alpha_min_; // Alpha value for gain matrix pseudo inverse.
    Eigen::MatrixXd vs_delta_gain_; // Delta values (min and max) for gain matrix pseudo inverse.
    Eigen::MatrixXd obs_inside_ir_sphere_; // Vector of obstacle inside sphere. If obstacle outside that dimension is 0;
    bool is_in_dist_zone_lin_; // If EE is in waypoint sphere (linear error check).
    bool is_in_dist_zone_ang_; // If EE is in waypoint sphere (angular error check).
    // UAM 
    Eigen::MatrixXd ini_uam_pos_; // Initial Quadrotor and arm joint values.
    Eigen::MatrixXd current_uam_pos_; // Current positions (quadrotor and arm joint). 
    Eigen::MatrixXd current_uam_vel_; // Current velocities (quadrotor and arm joint).
    Eigen::MatrixXd new_uam_pos_; // New positions (quadrotor and arm joint) computed using uam_vel obtained from algorithm. 

    // Quadrotor
    Eigen::MatrixXd current_quad_pos_; // Current pose of the quadrotor.
    Eigen::MatrixXd current_quad_vel_; // Current vel of the quadrotor.

    // Arm 
    KDL::Chain kdl_chain_; // Arm chain.
    Eigen::MatrixXd current_joint_pos_; // Current joint position from real robot.
    Eigen::MatrixXd current_joint_vel_; // Current joint velocity from real robot.
    Eigen::MatrixXd q_des_; // Desired arm positions for secondary task.

    bool enable_sec_task_; // Enable secondary task.

    Eigen::MatrixXd gain_sec_task_; // Gains of secondary tasks.
    Eigen::MatrixXd v_rollpitch_; // Current roll and pitch velocity of the quadrotor from the odometry.
    Eigen::MatrixXd q_rollpitch_; // Current quadrotor roll and pitch angles from the odometry.

    std::vector<UAM::CArmJoint> joint_info_; // Arm joints info.

    // Debug
    std::vector<float> debug_data_; // Vector to publish debug data.

    // Quadrotor output waypoints 
    int out_quad_wp_elements_; // To easy a new modification on the ee_waypoints_ number of elements.
    Eigen::MatrixXd out_quad_wp_; // List of waypoints column-wise. Elements are: x,y,z,yaw,cruise,max_lin_error,max_ang_error,wait.

    // EE input waypoints
    int in_ee_wp_elements_; // To easy a new modification on the ee_waypoints_ number of elements.
    Eigen::MatrixXd in_ee_wp_; // List of waypoints column-wise. Elements are: x,y,z,max_lin_error,wait.
    int curr_ee_wp_idx_; // Current targeted waypoint
    TaskStates task_state_; // EE waypoints Control task    
    double curr_ee_dist_error_; // Current euclidean linear distance to goal
    double curr_ee_ang_error_; // Current euclidean angular distance to goal
    ros::Time time_inpose_; // ROS time when arriving at waypoint
    Eigen::MatrixXd ee_last_pose_; // Last end-effector pose
    double ee_wp_time_ini_; // Initial time when new wp target was set (timeout)

    // [publisher attributes]
    ros::Publisher debug_data_publisher_;
    std_msgs::Float64MultiArray debug_data_msg_;

    ros::Publisher joint_traj_publisher_;
    trajectory_msgs::JointTrajectory joint_traj_msg_;

    ros::Publisher marker_array_publisher_;
    visualization_msgs::MarkerArray MarkerArray_msg_;

    // [subscriber attributes]
    ros::Subscriber joint_states_subscriber_;
    void joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg);
    pthread_mutex_t joint_states_mutex_;
    void joint_states_mutex_enter(void);
    void joint_states_mutex_exit(void);

    ros::Subscriber pose_subscriber_;
    void pose_callback(const geometry_msgs::TransformStamped::ConstPtr& msg);
    pthread_mutex_t pose_mutex_;
    void pose_mutex_enter(void);
    void pose_mutex_exit(void);

    // [service attributes]

    // [client attributes]

    // [action server attributes]
    IriActionServer<kinton_msgs::waypointsAction> in_ee_wp_aserver_;
    void in_ee_wpStartCallback(const kinton_msgs::waypointsGoalConstPtr& goal);
    void in_ee_wpStopCallback(void);
    bool in_ee_wpIsFinishedCallback(void);
    bool in_ee_wpHasSucceededCallback(void);
    void in_ee_wpGetResultCallback(kinton_msgs::waypointsResultPtr& result);
    void in_ee_wpGetFeedbackCallback(kinton_msgs::waypointsFeedbackPtr& feedback);
    bool in_ee_wp_action_active_;
    bool in_ee_wp_action_succeeded_;
    bool in_ee_wp_action_finished_;

    // [action client attributes]
    actionlib::SimpleActionClient<kinton_msgs::waypointsAction> waypoints_client_;
    kinton_msgs::waypointsGoal waypoints_goal_;
    bool waypointsMakeActionRequest();
    void waypointsDone(const actionlib::SimpleClientGoalState& state,  const kinton_msgs::waypointsResultConstPtr& result);
    void waypointsActive();
    void waypointsFeedback(const kinton_msgs::waypointsFeedbackConstPtr& feedback);

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    KintonArmConstrTaskOptAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~KintonArmConstrTaskOptAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]

    /**
     * \brief Read parameters
     *
     * In this function all needed parameters are read from parameters server.
     */
    void read_params(void);    

   /**
    * \brief Get Joints information
    *
    * Get Joints information 
    */
    bool read_joints(urdf::Model &robot_model,const std::string& root_name,const std::string& tip_name);

   /**
    * \brief Get Homogeneous Transform between cam and arm tip
    *
    * Get fixed Homogeneous Transform of the camera in tip frames
    */
    Eigen::Matrix4d get_HT_from_tf(const std::string& cam_frame, const std::string& tip_frame);    

   /**
    * \brief Get transform matrix
    *
    * From tf to Eigen 4x4 homogeneous transform
    */
    bool tf_to_eigen(const tf::Transform& transform, Eigen::Matrix4d& eigenT);

   /**
    * \brief Get transform matrix
    *
    * From Eigen 4x4 to tf homogeneous transform
    */
    void eigen_to_tf(const  Eigen::Matrix4d& eigenT, tf::Transform& transform);

   /**
    * \brief Rotation Matrix to euler angles
    *
    * Returns the euler angles from the input rotation matrix. It returns the closes angles to rpy_old
    */
    Eigen::Vector3d RToEulerContinuous(const Eigen::Matrix3d& Rot, const Eigen::Vector3d& rpy_old);

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  Eigen::Matrix4d& eigenT, const std::string& parent, const std::string& child);

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child);

   /**
    * \brief Arm Initialization
    *
    * Arm Variables initializations. 
    */
    void init_arm(void);

   /**
    * \brief Output velocity saturations
    *
    * Quadrotor linear and angular and arm joint angular velocity saturations
    */
    void output_vel_saturation(void);
    
   /**
    * \brief Simulate collision
    *
    * Outputs the distance to collision if a simulation of collision is required
    */
    void sim_cyl_collision(void);

   /**
    * \brief Simulate legs collision
    *
    * Computes leg points to simulate collision and publish marker  
    */
    void sim_legs_collision(void);

   /**
    * \brief Check if the end-effector is inside the confidence waypoint zone
    *
    * Check if the end-effector is inside the confidence waypoint zone defined by the user
    */    
    bool ee_inside_confidence_zone(const double& max_e_lin, const double& max_e_ang);

   /**
    * \brief Check if the quadrotor has waited in that position
    *
    * Check if the quadrotor has waited t_wait in that position
    */     
    bool hasWaited(const double& t_wait);

   /**
    * \brief Fill marker array message
    *
    * Visualization function to fill marker array publisher
    */
    void fill_marker_array(void);

    /**
    * \brief fill end-effector trajectory marker
    *
    * fill end-effector trajectory marker
    */  
    visualization_msgs::Marker fill_marker_ee_traj(void);

    /**
    * \brief Fill end-effector waypoint marker message
    *
    * Fill end-effector waypoint marker message
    */  
    visualization_msgs::Marker fill_ee_marker_waypoint(const int& ii);

    /**
    * \brief Fill end-effector waypoint error text marker message
    *
    * Fill end-effector waypoint error text marker message
    */  
    visualization_msgs::Marker fill_ee_marker_text_common(const int& ii);
    visualization_msgs::Marker fill_ee_marker_text_lin(const int& ii);
    visualization_msgs::Marker fill_ee_marker_text_ang(const int& ii);

    /**
    * \brief Fill leg markers
    *
    * Fill leg markers (with inflation radius)
    */     
    visualization_msgs::Marker fill_leg_marker(const int& ii, const int& leg_num, const Eigen::MatrixXd& points);

    /**
     * \brief Fill quadrotor waypoints message
     *
     * function to fill the quadrotor waypoints message
     */
    void fill_quad_wp(void);

    /**
     * \brief Fill joints trajectory message
     *
     * function to fill the joints trajectory message
     */
    void fill_joints_traj(void);    
};

#endif
