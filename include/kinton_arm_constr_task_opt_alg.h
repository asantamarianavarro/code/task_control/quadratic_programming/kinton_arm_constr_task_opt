// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// IMPORTANT NOTE: This code has been generated through a script from the
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _kinton_arm_constr_task_opt_alg_h_
#define _kinton_arm_constr_task_opt_alg_h_

#include <kinton_arm_constr_task_opt/KintonArmConstrTaskOptConfig.h>

#include <kdl_parser/kdl_parser.hpp>
#include <eigen3/Eigen/Dense>

#include <common_fc.h>
#include <common_obj.h>

#include <constr_task_opt/constr_task_opt.h>

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class KintonArmConstrTaskOptAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the KintonArmConstrTaskOptConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;

   /**
    * \brief Stop Quadrotor
    *
    * Stop quadrotor setting new problem bounds
    */ 
    bool StopQuad(const Eigen::VectorXd& pos_des, const double& d_min);

   /**
    * \brief Update Task Joint weights depending on distance to target
    *
    * Update Task Joint weights depending on distance to target
    */ 
    Eigen::VectorXd UpdateJointWeightsDist(const double& w_start, const double& w_end, const int& dim);

   /**
    * \brief Update Obstacle Avoidance Contraint
    *
    * Update Constraint with relative position of the robot w.r.t. obstacle.
    */ 
    void UpdateObsAvoConstr(const Eigen::MatrixXd& pquad_in_w, const int& constr_index);

    /**
    * \brief Update Obstacle Avoidance Contraint
    *
    * Update Constraint with relative position of the robot w.r.t. obstacle.
    */ 
    void UpdateLegsAvoConstr(const int& leg_num, const Eigen::MatrixXd& points, const int& constr_index);

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the KintonArmConstrTaskOptConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef kinton_arm_constr_task_opt::KintonArmConstrTaskOptConfig Config;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    KintonArmConstrTaskOptAlgorithm(void);

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    *
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void)
    {
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~KintonArmConstrTaskOptAlgorithm(void);

    UAM::CArm arm_;                         // Arm
    UAM::ArmCogData arm_cog_data_;          // Arm CoG data to debug in higher levels.
    UAM::CQuad quad_;                       // Quadrotor
    UAM::CUAM uam_;                         // Unmanned Aerial Manipulator.
    UAM::CCtrlParams ctrl_params_;          // Control related parameters
    UAM::CHT HT_;                           // Homogeneous transformations

    Eigen::MatrixXd eepos_des_;             // Desired end-effector position (x,y,z)

    CParams params_;                        // Low-level library object: Parameters.
    CBounds bounds_;                        // Optimization bounds (lower and upper for position, velocity and acceleration)
    std::vector<CTask> tasks_;              // Low-level library object: Tasks.
    std::vector<CConstr> constrs_;          // Low-level library object: Constraints.

    CConstrTaskOpt* OTG_ptr_;               // Low level constrained task optimization object.
        
    Eigen::VectorXd robot_acc_;             // Robot acceleration obtained with QP optimization
    Eigen::VectorXd robot_acc_dual_;        // Robot acceleration dual obtained with QP optimization

    Eigen::MatrixXd robot_pos_DEBUG_;       // Robot position variable used for feed-forward control
    Eigen::MatrixXd robot_vel_DEBUG_;       // Robot velocity variable used for feed-forward control
    bool first_time;                        // Variable used to initialize feed-forward control variables

    Eigen::MatrixXd obs_in_w_;              // Obstacle position in world coordinates
    double inf_radius_;                     // Inflation radius for security constraint

    Eigen::MatrixXd sigmaEEPOS_;            // EEPOS task error.
    Eigen::MatrixXd sigmaJL_;               // Joints limits task error.
    Eigen::MatrixXd sigmaG_;                // CoG alignement task error.
    Eigen::MatrixXd sigmaBASEPOS_;          // Base positioning task error.

    bool sim_legs_collision_;               // To simulate a collision with the legs.
    Eigen::MatrixXd legpoints_in_baselink_; // Leg points in baselink.
    Eigen::MatrixXd legpoints_in_armbase_;  // Leg points in arm base.
    std::vector<bool> leg_collision_;       // Leg collision detected.
    double leg_inf_radius_;                 // Leg inflation radius.
    Eigen::MatrixXd legs_act_dist_;         // Actual EE distance to Legs 
    bool sim_cyl_collision_;                // To simulate a collision to a vertical cylinder.
    bool coll_det_;                         // Obstacle collision detected.

    Eigen::VectorXd dist_from_goal_;        // Dsitance from goal to update weights.
    double d_min_goal_;                     // Minimum distance to goal (update weights).
    double d_max_goal_;                     // Minimum distance to goal (update weights).
    Eigen::VectorXd tEEPOS_weights_;        // EEPOS task weights.
    Eigen::VectorXd tCOG_weights_;          // COG task weights.
    Eigen::VectorXd JL_weights_;            // JL task weights.
    Eigen::VectorXd tMinVel_weights_;       // MinVel task weights.
    Eigen::VectorXd tBASEPOS_weights_;      // Base positioning task weights.

    /**
    * \brief Init Low-level Constrained Task Optimization library
    *
    * Init Low-level Constrained Task Optimization library.
    *
    */
    void init_constr_task_opt(const int& num_joints);

    /**
    * \brief Call Low-level Constrained Task Optimization library
    *
    * Call Low-level Constrained Task Optimization library.
    *
    */
    void const_task_opt(const Eigen:: MatrixXd& quad_dist_obs,
                        const double& stamp,
                        const Eigen::MatrixXd& current_uam_pos,
                        const Eigen::MatrixXd& current_uam_vel,
                        Eigen::MatrixXd& new_uam_pos,
                        std::vector<float>& debug_data);

    /**
    * \brief Fill debug data
    *
    * Fill all debug data into a vector.
    *
    */
    std::vector<float> fill_debug_data(const Eigen:: MatrixXd& quad_dist_obs,
                                       const double& stamp,
                                       const Eigen::MatrixXd& robot_pos,
                                       const Eigen::MatrixXd& robot_vel);    

};

#endif
