#include "kinton_arm_constr_task_opt_alg.h"

#include <kinematics.h>
#include <tasks/ir.h>
#include <tasks/cog.h>
#include <tasks/jl.h>
#include <tasks/eepos.h>

KintonArmConstrTaskOptAlgorithm::KintonArmConstrTaskOptAlgorithm(void)

{
  this->eepos_des_ = Eigen::MatrixXd::Zero(6,1);
  this->obs_in_w_ = Eigen::MatrixXd::Zero(3,2);
  this->inf_radius_ = 1.0;

  this->legpoints_in_baselink_ =   Eigen::MatrixXd::Zero(4,8);
  this->legpoints_in_armbase_ =   Eigen::MatrixXd::Zero(4,8);
  this->leg_collision_.resize(4);
  for (int ii = 0; ii < 4; ++ii)
    this->leg_collision_.at(ii) = false;
  this->leg_inf_radius_ = 0.0;
  this->legs_act_dist_ = Eigen::MatrixXd::Zero(3,4);

  this->coll_det_ = false;

  this->dist_from_goal_ = Eigen::VectorXd::Zero(3);
  d_min_goal_ = 0.0;
  d_max_goal_ = 0.0;  

  this->OTG_ptr_ = NULL;

  pthread_mutex_init(&this->access_,NULL);
}

KintonArmConstrTaskOptAlgorithm::~KintonArmConstrTaskOptAlgorithm(void)
{
  if (this->OTG_ptr_ != NULL)
  {
    delete this->OTG_ptr_;
    this->OTG_ptr_ = NULL;
  }

  pthread_mutex_destroy(&this->access_);
}

void KintonArmConstrTaskOptAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;

  this->unlock();
}

// KintonArmConstrTaskOptAlgorithm Public API

void KintonArmConstrTaskOptAlgorithm::init_constr_task_opt(const int& num_joints)
{
  // Fill CONSTRAINTS Objects __________________________________
  this->constrs_.clear();

  // Obstacle Avoidance
  int dimConstrIR = 1;
  bool CIR_is_pos = true;
  int CIR_bound_type = 0; /** 0 if 0<f, 1 if f<0, 2 if 0=f=0, any other number deactivates the constraint*/
  CConstr ConstrIR(dimConstrIR,this->params_.num_vars, CIR_is_pos, CIR_bound_type);
  ConstrIR.error = Eigen::VectorXd::Zero(ConstrIR.dim);
  ConstrIR.Jac = Eigen::MatrixXd::Zero(ConstrIR.dim, this->params_.num_vars);
  this->constrs_.push_back(ConstrIR);

  int dimConstrLeg = 1;
  bool CLeg_is_pos = true;
  int CLeg_bound_type = 0; /** 0 if 0<f, 1 if f<0, 2 if 0=f=0, any other number deactivates the constraint*/
  CConstr ConstrlEG(dimConstrLeg,this->params_.num_vars, CLeg_is_pos, CLeg_bound_type);
  ConstrlEG.error = Eigen::VectorXd::Zero(ConstrlEG.dim);
  ConstrlEG.Jac = Eigen::MatrixXd::Zero(ConstrlEG.dim, this->params_.num_vars);
  for (int ii = 0; ii < this->leg_collision_.size(); ++ii)
    this->constrs_.push_back(ConstrlEG);

  this->params_.num_constr = 0;
  for (int ii = 0; ii < this->constrs_.size(); ++ii)
    this->params_.num_constr = this->params_.num_constr + this->constrs_[ii].dim;

  // Fill BOUNDS Object ______________________________________
  Eigen::VectorXd arm_lpos(this->arm_.nj);
  Eigen::VectorXd arm_upos(this->arm_.nj);
  for (int ii = 0; ii < 6; ++ii)
  { 
    arm_lpos(ii) = this->arm_.joint_info.at(ii).joint_min;
    arm_upos(ii) = this->arm_.joint_info.at(ii).joint_max;
  }
  this->bounds_.lpos = Eigen::VectorXd::Zero(this->params_.num_vars);
  this->bounds_.upos = Eigen::VectorXd::Zero(this->params_.num_vars);
  this->bounds_.lvel = Eigen::VectorXd::Zero(this->params_.num_vars);
  this->bounds_.uvel = Eigen::VectorXd::Zero(this->params_.num_vars);
  this->bounds_.lacc = Eigen::VectorXd::Zero(this->params_.num_vars);
  this->bounds_.uacc = Eigen::VectorXd::Zero(this->params_.num_vars);

  this->bounds_.lpos << -2.0,-2.5,0.2,-2*3.14159*10e20,arm_lpos;
  this->bounds_.upos << 2.0,1.5,2.0,2*3.14159*10e20,arm_upos;
  this->bounds_.lvel << -0.1,-0.1,-0.1,-0.1,-0.5,-0.5,-0.5,-0.5,-0.5,-0.5;
  this->bounds_.uvel = -this->bounds_.lvel;
  this->bounds_.lacc = this->params_.l2*this->bounds_.lvel;
  this->bounds_.uacc = this->params_.l2*this->bounds_.uvel;

  // Initialize MISC objects and parameters __________________
  this->OTG_ptr_ = new CConstrTaskOpt(this->params_.num_vars, this->params_.num_constr, this->params_.nWSR);
  this->OTG_ptr_->set_parameters(this->params_, this->bounds_);

  this->robot_acc_ = Eigen::VectorXd::Zero(4+this->arm_.nj);
  this->robot_acc_dual_ = Eigen::VectorXd::Zero(4+this->arm_.nj+this->params_.num_constr);

  first_time = true;

  // Initialize task variables
  this->sigmaEEPOS_ = Eigen::MatrixXd::Zero(6,1);
  this->sigmaJL_ = Eigen::MatrixXd::Zero(this->arm_.nj,1);
  this->sigmaG_ = Eigen::MatrixXd::Zero(2,1);

  this->tEEPOS_weights_ = Eigen::VectorXd::Zero(this->sigmaEEPOS_.rows());
  this->JL_weights_ = Eigen::VectorXd::Zero(this->sigmaJL_.rows());
  this->tCOG_weights_ = Eigen::VectorXd::Zero(this->sigmaG_.rows());
  this->tMinVel_weights_ = Eigen::VectorXd::Zero(4+this->arm_.nj);
}

void KintonArmConstrTaskOptAlgorithm::const_task_opt(const Eigen:: MatrixXd& quad_dist_obs,
                                                     const double& stamp,
                                                     const Eigen::MatrixXd& current_uam_pos,
                                                     const Eigen::MatrixXd& current_uam_vel,
                                                     Eigen::MatrixXd& new_uam_pos,
                                                     std::vector<float>& debug_data)
{
  // UAM Positions ____________________________________________
  if (this->first_time) 
  {
    this->robot_pos_DEBUG_ = Eigen::MatrixXd::Zero(4+this->arm_.nj,1);
    this->robot_vel_DEBUG_ = Eigen::MatrixXd::Zero(4+this->arm_.nj,1);
    this->robot_pos_DEBUG_.block(0,0,3,1) = current_uam_pos.block(0,0,3,1);
    this->robot_pos_DEBUG_.block(3,0,1+this->arm_.nj,1) = current_uam_pos.block(5,0,1+this->arm_.nj,1);
    this->first_time = false;
  }
  // Feed-Forward control (open loop)
  this->quad_.pos = Eigen::MatrixXd::Zero(6,1);
  this->quad_.pos << this->robot_pos_DEBUG_.block(0,0,3,1), Eigen::MatrixXd::Zero(2,1), this->robot_pos_DEBUG_.block(3,0,1,1);
  this->arm_.jnt_pos = this->robot_pos_DEBUG_.block(4,0,this->arm_.nj,1);
  // Feed-Back control (closed loop)
  // this->quad_.pos = current_uam_pos.block(0,0,6,1);
  // this->arm_.jnt_pos = current_uam_pos.block(6,0,this->arm_.nj,1);

  // UAM Kinematics ___________________________________________
  UAM::CKinematics::UAMKine(this->quad_,this->arm_,this->HT_,this->uam_);

  // TASK: CoG alignement _____________________________________
  Eigen::MatrixXd JG,JG_pseudo;
  UAM::CTaskCOG::TaskErrorJacFull(this->arm_,this->HT_,this->ctrl_params_.cog_PGxy,this->ctrl_params_.cog_arm,this->sigmaG_,JG,JG_pseudo,this->arm_cog_data_);

  // TASK: Joint limits _______________________________________
  Eigen::MatrixXd JJL,JJL_pseudo;
  UAM::CTaskJL::TaskErrorJacFull(this->arm_, this->ctrl_params_.jntlim_pos_des, this->ctrl_params_.jntlim_pos_error, this->sigmaJL_, JJL, JJL_pseudo);

  // TASK: End-Effector Positioning ___________________________
  Eigen::MatrixXd JEEPOS,JEEPOS_pseudo;
  UAM::CTaskEEPOS::TaskErrorJac(this->HT_, this->arm_, this->ctrl_params_.vs_delta_gain, this->ctrl_params_.vs_alpha_min, this->eepos_des_, this->sigmaEEPOS_, JEEPOS, JEEPOS_pseudo);

  // Fill TASKs Object ________________________________________
  this->tasks_.clear();

  // End-Effector positioning task
  int dimEEPOS = this->sigmaEEPOS_.rows();
  bool tEEPOS_is_pos = true;
  bool tEEPOS_norm_jac = true;
  this->tEEPOS_weights_ = 5e1*Eigen::VectorXd::Ones(dimEEPOS); // Acting allways (but JL weights are bigger during navigation phase)
  CTask taskEEPOS(dimEEPOS, 4+this->arm_.nj, tEEPOS_is_pos, tEEPOS_norm_jac, this->tEEPOS_weights_);
  taskEEPOS.error = this->sigmaEEPOS_.col(0);
  taskEEPOS.Jac = JEEPOS;
  this->tasks_.push_back(taskEEPOS);

  // CoG alignement 
  int dimCOG = this->sigmaG_.rows();
  bool tCOG_is_pos = true;
  bool tCOG_norm_jac = true;
  this->tCOG_weights_ = UpdateJointWeightsDist(1e0,0.0,dimCOG); // CoG acting only during navigation phase
  CTask taskCOG(dimCOG,4+this->arm_.nj, tCOG_is_pos, tCOG_norm_jac, this->tCOG_weights_);
  taskCOG.error = this->sigmaG_.col(0);
  taskCOG.Jac = JG;
  this->tasks_.push_back(taskCOG);

  // Arm Joint Limits: joint's equilibrium position
  int dimJL = this->sigmaJL_.rows();
  bool JL_is_pos = true;
  bool JL_norm_jac = true;
  this->JL_weights_ = UpdateJointWeightsDist(1e8,0.0,dimJL); // JL acting only during navigation phase
  CTask taskJL(dimJL,4+this->arm_.nj, JL_is_pos, JL_norm_jac, this->JL_weights_);
  taskJL.error = this->sigmaJL_.col(0);
  taskJL.Jac = JJL;
  this->tasks_.push_back(taskJL);
  
  // Minimize Velocity
  int dimMinVel = 4+this->arm_.nj;
  bool tMinVel_is_pos = false;
  bool tMinVel_norm_jac = true;
  this->tMinVel_weights_.segment(0,1) = UpdateJointWeightsDist(0.0,1e4,1); // Block Quad X before goal
  this->tMinVel_weights_.segment(3,this->arm_.nj) = UpdateJointWeightsDist(1e4,0.0,this->arm_.nj); // Allow joints move at goal
  this->tMinVel_weights_.segment(4,2) = UpdateJointWeightsDist(0.0,1e8,2); // Block conflicting joints at goal
  CTask taskMinVel(dimMinVel,4+this->arm_.nj, tMinVel_is_pos, tMinVel_norm_jac, this->tMinVel_weights_);
  taskMinVel.error = Eigen::MatrixXd::Zero(4+this->arm_.nj,1) - this->robot_vel_DEBUG_;
  taskMinVel.Jac = Eigen::MatrixXd::Identity(dimMinVel,4+this->arm_.nj);
  this->tasks_.push_back(taskMinVel);

  // Fill Constraints _________________________________________
  // Obstacle collision avoidance
  if (this->sim_cyl_collision_)
    UpdateObsAvoConstr(robot_pos_DEBUG_.block(0,0,3,1),0); // USE OF REFERENCE POSITION
    // UpdateObsAvoConstr(current_uam_pos.block(0,0,3,1),0); // USE OF FEEDBACK POSITION

  // Legs collision avoidance
  if (this->sim_legs_collision_)
  {
    for (int ii = 0; ii < 4; ++ii)
      UpdateLegsAvoConstr(ii,this->legpoints_in_baselink_,ii+1);
  }

  // Set Tasks ________________________________________________
  this->OTG_ptr_->set_tasks(this->tasks_);
  
  // Set Constraints __________________________________________
  this->OTG_ptr_->set_constraints(this->constrs_);
  
  // Set joints values (pos, vel and weights) _________________
  Eigen::VectorXd joints_weights = Eigen::VectorXd::Ones(this->params_.num_vars);
  this->OTG_ptr_->set_joints(this->robot_pos_DEBUG_, this->robot_vel_DEBUG_, joints_weights); // Feed-Forward control (open loop)
  
  // Solve ____________________________________________________
  Eigen::MatrixXd robot_pos_ref = Eigen::MatrixXd::Zero(4+this->arm_.nj,1);
  Eigen::MatrixXd robot_vel_ref = Eigen::MatrixXd::Zero(4+this->arm_.nj,1);
  this->OTG_ptr_->call_solver(this->robot_acc_, this->robot_acc_dual_, robot_vel_ref, robot_pos_ref);
  
  // Set Alg outputs __________________________________________
  new_uam_pos = Eigen::MatrixXd::Zero(6+this->arm_.nj,1);
  new_uam_pos.block(0,0,3,1) = robot_pos_ref.block(0,0,3,1);
  new_uam_pos.block(5,0,1+this->arm_.nj,1) = robot_pos_ref.block(3,0,1+this->arm_.nj,1);
  
  // Feed-Forward control (open loop). Update variables _______
  this->robot_pos_DEBUG_ = robot_pos_ref;
  this->robot_vel_DEBUG_ = robot_vel_ref;
  
  // recompute UAM Kinematics. Alg output requirements ________
  this->quad_.pos = current_uam_pos.block(0,0,6,1);
  this->arm_.jnt_pos = current_uam_pos.block(6,0,this->arm_.nj,1);
  UAM::CKinematics::UAMKine(this->quad_,this->arm_,this->HT_,this->uam_);
  
  // Fill DEBUG data __________________________________________
  debug_data = fill_debug_data(quad_dist_obs,stamp,current_uam_pos,current_uam_vel);
}

bool KintonArmConstrTaskOptAlgorithm::StopQuad(const Eigen::VectorXd& pos_des, const double& d_min)
{
  bool stopped = false;
  Eigen::VectorXd dist_from_goal = pos_des-this->HT_.baselink_in_w.block(0,3,3,1);

  // Set bounds if required
  CBounds new_bounds = this->bounds_;
  if (dist_from_goal.norm() < d_min)
  {
    for (int ii = 0; ii < 1; ++ii)
    {
      if (this->eepos_des_(ii) < this->HT_.baselink_in_w(ii,3))
        new_bounds.lpos(ii) = this->HT_.baselink_in_w(ii,3);
      else
        new_bounds.upos(ii) = this->HT_.baselink_in_w(ii,3);
    }
    stopped = true;
  }
  this->OTG_ptr_->set_bounds(new_bounds);
  return stopped;
}

Eigen::VectorXd KintonArmConstrTaskOptAlgorithm::UpdateJointWeightsDist(const double& w_start, const double& w_end,const int& dim)
{
  // Distance parameters
  this->d_min_goal_ = 0.30; // Distance smaller than this -> 0
  this->d_max_goal_ = 0.50; // Distance bigger than this -> w_start, smaller -> w_end    

  // Update currently based on distance to Waypoint
  this->dist_from_goal_ = this->eepos_des_.block(0,0,3,1)-this->HT_.baselink_in_w.block(0,3,3,1);

  Eigen::VectorXd weights = w_start*Eigen::VectorXd::Ones(dim);

  double w_actual;

  if (this->dist_from_goal_.norm() > this->d_max_goal_)
    w_actual = w_start;
  else 
  {
    if(this->dist_from_goal_.norm() < this->d_min_goal_)
      w_actual = w_end;
    else
    {
      double tmp = 3.14159*(2*( -(this->dist_from_goal_.norm()-this->d_min_goal_)/(this->d_max_goal_-this->d_min_goal_) )-1);
      double alpha = 0.5*(tanh(tmp)+1); 
      w_actual =  w_start + alpha*(w_end-w_start);
    }
  }

  for (unsigned int ii = 0; ii < dim; ++ii)
    weights(ii) = w_actual;

  return weights;
}

void KintonArmConstrTaskOptAlgorithm::UpdateObsAvoConstr(const Eigen::MatrixXd& pquad_in_w, const int& constr_index)
{
  // Inputs
  Eigen::VectorXd min_dist = (this->inf_radius_+this->obs_in_w_(0,1)/2.0)*Eigen::VectorXd::Ones(1); 
  // Eigen::VectorXd min_dist = this->inf_radius_*Eigen::VectorXd::Ones(1); 
  Eigen::MatrixXd pos_int = pquad_in_w.block(0,0,2,1); // Interesting point position (pos_quad) // 2D constraint: Considering vertical obstacle
  Eigen::MatrixXd pos_obs = this->obs_in_w_.block(0,0,2,1); // Obstacle position

  // Constraint error: e = (current_xypos-obst_xypos)^2 - inf_radius^2 > 0   
  Eigen::VectorXd constr_error = Eigen::VectorXd::Zero(1);
  constr_error(0) = -min_dist(0)*min_dist(0); 
  for (int ii = 0; ii < pos_int.rows(); ++ii)
    constr_error(0) = constr_error(0) + (pos_int(ii)-pos_obs(ii))*(pos_int(ii)-pos_obs(ii));    

  // Constraint Jac: [2*e^T*dpdq zeros(1+arm.nj)] 
  Eigen::MatrixXd dpos_int_dq = Eigen::MatrixXd::Zero(pos_int.rows(),4+this->arm_.nj);
  dpos_int_dq.block(0,0,pos_int.rows(),pos_int.rows()) = Eigen::MatrixXd::Identity(pos_int.rows(),pos_int.rows());
  Eigen::MatrixXd constr_Jac = Eigen::MatrixXd::Zero(1,4+this->arm_.nj); // number_obst X number_joints; 
  constr_Jac =  2*(pos_int-pos_obs).transpose()*dpos_int_dq; 

  // Fill object in constraints object
  this->constrs_.at(constr_index).error = constr_error; // This minus is to be coherent with task notation.
  this->constrs_.at(constr_index).Jac = constr_Jac;

  // Check inflation radius invasion
  this->coll_det_ = false;
  if (constr_error(0) < 0.0)
    this->coll_det_ = true;
}

void KintonArmConstrTaskOptAlgorithm::UpdateLegsAvoConstr(const int& leg_num, const Eigen::MatrixXd& points, const int& constr_index)
{
  // Inflation radius for each leg
  this->leg_inf_radius_ = 0.02;

  // Two points lying in the leg
  Eigen::VectorXd p1 = points.block(0,2*leg_num,3,1);
  Eigen::VectorXd p2 = points.block(0,2*leg_num+1,3,1);

  // Compute point-line distance
  Eigen::VectorXd n_k = Eigen::VectorXd::Zero(3);  
  n_k = (p2-p1)/(p2-p1).norm(); // Versor directed as the leg
  Eigen::MatrixXd D_projection = Eigen::MatrixXd::Zero(3,3);  // Projection matrix
  D_projection = Eigen::MatrixXd::Identity(3,3) - n_k*n_k.transpose(); 
  Eigen::MatrixXd pos_int = this->HT_.cam_in_baselink.block(0,3,3,1); // Interesting point position (pos_ee with respect arm base frame)
  this->legs_act_dist_.col(leg_num) = D_projection*(p1-pos_int);
  
  // Constraint error: e = d_leg_actual^2 - d_min^2 > 0   
  Eigen::VectorXd constr_error = Eigen::VectorXd::Zero(1);
  Eigen::VectorXd min_dist = this->leg_inf_radius_*Eigen::VectorXd::Ones(1);  
  constr_error(0) = - min_dist(0)*min_dist(0);
  for (int ii = 0; ii < pos_int.rows(); ++ii)
     constr_error(0) = constr_error(0) + this->legs_act_dist_(ii,leg_num)*this->legs_act_dist_(ii,leg_num);    

  // Constraint Jac: [Ixy*2*e zeros(1+arm.nj)] 
  Eigen::MatrixXd ArmJac = UAM::CKinematics::ArmJac(this->arm_);
  Eigen::MatrixXd JEEPOS_body = Eigen::MatrixXd::Zero(3,4+this->arm_.nj);
  JEEPOS_body.block(0,4,3,this->arm_.nj) = this->HT_.armbase_in_baselink.block(0,0,3,3)*ArmJac.block(0,0,3,this->arm_.nj);
  Eigen::MatrixXd constr_Jac = Eigen::MatrixXd::Zero(1,4+this->arm_.nj); // number_obst X number_joints; 
  constr_Jac =  2*this->legs_act_dist_.col(leg_num).transpose()*JEEPOS_body; 

  // Fill object in constraints object
  this->constrs_.at(constr_index).error = constr_error; // This minus is to be coherent with task notation.
  this->constrs_.at(constr_index).Jac = constr_Jac;

  // Set collision detected object
  this->leg_collision_.at(leg_num) = false;
  if (constr_error(0) < 0.0)
  {
    this->leg_collision_.at(leg_num) = true;
    std::cout << "WARNING -> Collision in leg: " << leg_num << " Constr. error: [" << constr_error << "]" << std::endl;
  }
}

std::vector<float> KintonArmConstrTaskOptAlgorithm::fill_debug_data(const Eigen:: MatrixXd& quad_dist_obs,
                                                                    const double& stamp,
                                                                    const Eigen::MatrixXd& robot_pos,
                                                                    const Eigen::MatrixXd& robot_vel)
{
  // Store debug data
  std::vector<float> debug_data;
  debug_data.clear();

  // AUXILIAR DATA ______________________________
  // Arm torque
  double pcog_in_baselink = sqrt(this->ctrl_params_.cog_arm(0,0)*this->ctrl_params_.cog_arm(0,0)+
    this->ctrl_params_.cog_arm(1,0)*this->ctrl_params_.cog_arm(1,0));
  double dot_product = this->ctrl_params_.cog_arm(0,0);
  double angle = asin(dot_product/pcog_in_baselink);
  double total_arm_mass = 1.8326;
  Eigen::MatrixXd torque = Eigen::MatrixXd::Zero(2,1);
  torque(0,0) = pcog_in_baselink * total_arm_mass * sin(-angle);
  torque(1,0) = pcog_in_baselink * total_arm_mass * cos(-angle);

  // ROS ________________________________________
  debug_data.push_back((float)stamp);

  // ROBOT & KINEMATICS _________________________
  // pose
  for (int ii = 0; ii < robot_pos.rows(); ++ii)
    debug_data.push_back((float)robot_pos(ii,0));
  // velocity
  for (int ii = 0; ii < robot_vel.rows(); ++ii)
    debug_data.push_back((float)robot_vel(ii,0));
  // pose_DEBUG
  for (int ii = 0; ii < this->robot_pos_DEBUG_.rows(); ++ii)
    debug_data.push_back((float)this->robot_pos_DEBUG_(ii,0));
  // velocity_DEBUG
  for (int ii = 0; ii < this->robot_vel_DEBUG_.rows(); ++ii)
    debug_data.push_back((float)this->robot_vel_DEBUG_(ii,0));
  // Acceleration_ref
  for (int ii = 0; ii < this->robot_acc_.rows(); ++ii)
    debug_data.push_back((float)this->robot_acc_(ii,0));
  // Acceleration_ref_dual
  for (int ii = 0; ii < this->robot_acc_dual_.rows(); ++ii)
    debug_data.push_back((float)this->robot_acc_dual_(ii,0));

  // TASKS_______________________________________
  // EEPOS _____
  // Desired eepos
  for (int ii = 0; ii < this->eepos_des_.rows(); ++ii)
    debug_data.push_back((float)this->eepos_des_(ii,0));
  // Sigma_eepos
  for (int ii = 0; ii < this->sigmaEEPOS_.rows(); ++ii)
    debug_data.push_back((float)this->sigmaEEPOS_(ii,0));  
 
  // COG _______
  // Arm CoG
  for (int ii = 0; ii < this->ctrl_params_.cog_arm.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.cog_arm(ii,0));
  // Arm torque
  for (int ii = 0; ii < torque.rows(); ++ii)
    debug_data.push_back((float)torque(ii,0));
  // Sigma_CoG
  for (int ii = 0; ii < this->sigmaG_ .rows(); ++ii)
    debug_data.push_back((float)this->sigmaG_ (ii,0));  
  
  // JL ________
  // Desired joint positions
  for (int ii = 0; ii < this->ctrl_params_.jntlim_pos_des.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.jntlim_pos_des(ii,0));
  // Sigma_JL
  for (int ii = 0; ii < this->sigmaJL_.rows(); ++ii)
    debug_data.push_back((float)this->sigmaJL_(ii,0));    

  // CONSTRAINTS_________________________________
  // Inflation radius
  debug_data.push_back((float)this->ctrl_params_.inf_radius);
  // Obstacle position in world
  for (int ii = 0; ii < this->obs_in_w_.rows(); ++ii)
    debug_data.push_back((float)this->obs_in_w_(ii,0));
  // Legs Inflation Radius
  debug_data.push_back((float)this->leg_inf_radius_);
  // EE Distance to legs
  for (int jj = 0; jj < this->legs_act_dist_.cols(); ++jj)
    for (int ii = 0; ii < this->legs_act_dist_.rows(); ++ii)
      debug_data.push_back((float)this->legs_act_dist_(ii,jj));
  // Enabled Obstacle avoidance
  debug_data.push_back((float)this->sim_cyl_collision_);
  // Enabled legs avoidance
  debug_data.push_back((float)this->sim_legs_collision_);

  // WEIGHTS UPDATE ___________________________
  // distance from goal  
  for (int ii = 0; ii < this->dist_from_goal_.rows(); ++ii)
    debug_data.push_back((float)this->dist_from_goal_(ii));
  // minimum distance to goal
  debug_data.push_back((float)this->d_min_goal_);
  // maximum distance to goal
  debug_data.push_back((float)this->d_max_goal_);
  // EEPOS weights
  for (int ii = 0; ii < this->tEEPOS_weights_.rows(); ++ii)
    debug_data.push_back((float)this->tEEPOS_weights_(ii));
  // COG weights
  for (int ii = 0; ii < this->tCOG_weights_.rows(); ++ii)
    debug_data.push_back((float)this->tCOG_weights_(ii));
  // JL weights
  for (int ii = 0; ii < this->JL_weights_.rows(); ++ii)
    debug_data.push_back((float)this->JL_weights_(ii));
  // Minvel weights
  for (int ii = 0; ii < this->tMinVel_weights_.rows(); ++ii)
    debug_data.push_back((float)this->tMinVel_weights_(ii));      

  return debug_data;
}
