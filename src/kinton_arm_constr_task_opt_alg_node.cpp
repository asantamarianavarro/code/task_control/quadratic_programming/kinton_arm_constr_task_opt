#include "kinton_arm_constr_task_opt_alg_node.h"

KintonArmConstrTaskOptAlgNode::KintonArmConstrTaskOptAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<KintonArmConstrTaskOptAlgorithm>(),
  in_ee_wp_aserver_(public_node_handle_, "in_ee_waypoints"),
  waypoints_client_("waypoints", true)
{
  //init class attributes if necessary

  double Hz = 50.0;
  this->loop_rate_ = Hz;//in [Hz]
  this->alg_.params_.tstep = 1.0/Hz;

  // Initialize values
  this->alg_.sim_cyl_collision_ = true;
  this->alg_.sim_legs_collision_ = true;

  this->init_ = true;
  this->time_ini_ = ros::Time::now();
  this->curr_t_ = 0.0;
  this->dt_ = 0.0;

  this->v_rollpitch_ = Eigen::MatrixXd::Zero(2, 1);
  this->q_rollpitch_ = Eigen::MatrixXd::Zero(2, 1);
  this->gain_sec_task_ = Eigen::MatrixXd::Zero(3, 1);

  this->Ttag_in_cam_ = Eigen::Matrix4d::Identity();
  this->Tcam_in_tip_ = Eigen::Matrix4d::Identity();

  // Output Quadrotor Waypoints info
  this->out_quad_wp_elements_ = 8; // Elements are: x,y,z,yaw,cruise,max_lin_error,max_ang_error,wait.
  this->out_quad_wp_ = Eigen::MatrixXd::Zero(this->out_quad_wp_elements_,1);

  // Input EE Waypoints info
  this->in_ee_wp_elements_ = 9; // Elements are: x,y,z,roll,pitch,yaw,max_lin_error,max_ang_error,wait.
  this->in_ee_wp_ = Eigen::MatrixXd::Zero(this->in_ee_wp_elements_,1);
  this->curr_ee_wp_idx_ = 0;
  this->task_state_ = IDLE;
  this->curr_ee_dist_error_ = 0.0;
  this->curr_ee_ang_error_ = 0.0;
  this->time_inpose_ = this->time_ini_;
  this->ee_last_pose_ = Eigen::MatrixXd::Zero(3,1);
  this->ee_wp_time_ini_ = 0.0;
  this->is_in_dist_zone_lin_ = false;
  this->is_in_dist_zone_ang_ = false;

  // Read parameters from ROS parameters server
  read_params();

  this->current_quad_pos_ = Eigen::MatrixXd::Zero(6,1);
  this->current_quad_vel_ = Eigen::MatrixXd::Zero(6,1);
  this->current_joint_pos_ = Eigen::MatrixXd::Zero(this->alg_.arm_.nj,1);
  this->current_joint_vel_ = Eigen::MatrixXd::Zero(this->alg_.arm_.nj,1);
  this->current_uam_pos_ = Eigen::MatrixXd::Zero(6+this->alg_.arm_.nj,1);
  this->current_uam_vel_ = Eigen::MatrixXd::Zero(6+this->alg_.arm_.nj,1);

  this->new_uam_pos_ = Eigen::MatrixXd::Zero(6+this->alg_.arm_.nj,1);

  // [init publishers]
  this->debug_data_publisher_ = this->public_node_handle_.advertise<std_msgs::Float64MultiArray>("debug_data", 1);
  this->joint_traj_publisher_ = this->public_node_handle_.advertise<trajectory_msgs::JointTrajectory>("joint_traj", 1);
  this->marker_array_publisher_ = this->public_node_handle_.advertise<visualization_msgs::MarkerArray>("marker_array", 1);

  // [init subscribers]
  this->joint_states_subscriber_ = this->public_node_handle_.subscribe("joint_states", 1, &KintonArmConstrTaskOptAlgNode::joint_states_callback, this);
  pthread_mutex_init(&this->joint_states_mutex_,NULL);

  this->pose_subscriber_ = this->public_node_handle_.subscribe("pose", 1, &KintonArmConstrTaskOptAlgNode::pose_callback, this);
  pthread_mutex_init(&this->pose_mutex_,NULL);

  // [init services]

  // [init clients]

  // [init action servers]
  in_ee_wp_aserver_.registerStartCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpStartCallback, this, _1));
  in_ee_wp_aserver_.registerStopCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpStopCallback, this));
  in_ee_wp_aserver_.registerIsFinishedCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpIsFinishedCallback, this));
  in_ee_wp_aserver_.registerHasSucceedCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpHasSucceededCallback, this));
  in_ee_wp_aserver_.registerGetResultCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpGetResultCallback, this, _1));
  in_ee_wp_aserver_.registerGetFeedbackCallback(boost::bind(&KintonArmConstrTaskOptAlgNode::in_ee_wpGetFeedbackCallback, this, _1));
  in_ee_wp_aserver_.start();
  this->in_ee_wp_action_active_=false;
  this->in_ee_wp_action_succeeded_=false;
  this->in_ee_wp_action_finished_=false;

  // [init action clients]

  // Init low level library
  this->alg_.init_constr_task_opt(this->alg_.arm_.nj);

  if (this->alg_.sim_legs_collision_)
    sim_legs_collision();   
  if (this->alg_.sim_cyl_collision_)
    sim_cyl_collision();   
}

KintonArmConstrTaskOptAlgNode::~KintonArmConstrTaskOptAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->pose_mutex_);
  pthread_mutex_destroy(&this->joint_states_mutex_);
}

void KintonArmConstrTaskOptAlgNode::mainNodeThread(void)
{
  // TODO: do not use the same variable for uam_pos and uam_ref
  // Currently at the beginning of the loop is uam_pos and at the end is uam_ref.

  // get real robot positions
  this->pose_mutex_enter();
  this->current_uam_pos_.block(0,0,6,1) = this->current_quad_pos_;
  this->current_uam_vel_.block(0,0,6,1) = this->current_quad_vel_;
  this->pose_mutex_exit();
  this->joint_states_mutex_enter();
  this->current_uam_pos_.block(6,0,this->alg_.arm_.nj,1) = this->current_joint_pos_;
  this->current_uam_vel_.block(6,0,this->alg_.arm_.nj,1) = this->current_joint_vel_;
  this->joint_states_mutex_exit();

  // Class variables to local
  this->alg_.lock();
  this->curr_t_ = (ros::Time::now()-this->time_ini_).toSec();
  double curr_t = this->curr_t_;
  bool init = this->init_;
  bool in_ee_wp_action_active = this->in_ee_wp_action_active_;
  bool in_ee_wp_action_finished = this->in_ee_wp_action_finished_;
  this->alg_.unlock();

  // Initialize arm and time
  if (init)
  {
    // Initialize arm joints
    init_arm();

    // Initialize robot velocities
    this->current_uam_vel_ = Eigen::MatrixXd::Zero(6 + this->alg_.arm_.nj, 1);

    // Fill arm joints trajectory
    fill_joints_traj();
    // Publish 10 times to be sure the conection has been created.
    this->joint_traj_publisher_.publish(this->joint_traj_msg_);

    // Keep sending initial joint position for 5 sec to wait until other nodes.
    if (curr_t > 5.0)
      this->init_ = false;
  }
  else
  {
    switch (this->task_state_)
    {
      case IDLE:
        this->curr_ee_wp_idx_ = 0;
        if (in_ee_wp_action_active && !in_ee_wp_action_finished)
        {
          this->task_state_ = NAVIGATING;
          this->ee_wp_time_ini_ = curr_t;
        }
      break;
      case NAVIGATING:
        if (ee_inside_confidence_zone(this->in_ee_wp_(6,this->curr_ee_wp_idx_),this->in_ee_wp_(7,this->curr_ee_wp_idx_)))
        {
          this->task_state_ = INWAYPOINT;
          this->time_inpose_ = ros::Time::now();
        }
        else
        {
          // To finish the action server with failure
          if (curr_t - this->ee_wp_time_ini_ > 120.0)//
          {
            this->alg_.lock();
            this->in_ee_wp_action_succeeded_=false;
            this->in_ee_wp_action_finished_=true;
            this->in_ee_wp_action_active_=false;
            this->init_ = true;
            this->task_state_ = IDLE;
            ROS_WARN("EEPOS Action finished by time-out.");
            this->alg_.unlock();
          }
        }
      break;
      case INWAYPOINT:
        if (hasWaited(this->in_ee_wp_(8,this->curr_ee_wp_idx_)))
        {
          if (this->curr_ee_wp_idx_ == this->in_ee_wp_.cols()-1)
          {
            // To finish the action server with success
            this->alg_.lock();
            this->in_ee_wp_action_succeeded_=true;
            this->in_ee_wp_action_finished_=true;
            this->in_ee_wp_action_active_=false;
            this->init_ = true;
            this->task_state_ = IDLE;
            ROS_INFO("[kinton_arm_constr_task_opt]: EE wp action finished successfully!");
            this->alg_.unlock();
          }
          else
          {
            this->curr_ee_wp_idx_ = this->curr_ee_wp_idx_ + 1;
            this->task_state_ = NAVIGATING;
            this->ee_wp_time_ini_ = curr_t;
            ROS_INFO("[kinton_arm_constr_task_opt]: EE Waypoint reached. Going to next waypoint.");
          }
        }
        else
        {
          if (!ee_inside_confidence_zone(this->in_ee_wp_(6,this->curr_ee_wp_idx_),this->in_ee_wp_(7,this->curr_ee_wp_idx_)))
          {
            this->task_state_ = NAVIGATING;
            ROS_INFO("[kinton_arm_constr_task_opt]: EE Waypoint lost. Linear error too high!");
          }
        }
      break;
    }

    // Update last pose
    this->alg_.lock();
    this->ee_last_pose_ = this->alg_.HT_.cam_in_w.block(0,3,3,1);
    this->alg_.unlock();

    if (this->task_state_ == NAVIGATING || this->task_state_ == INWAYPOINT)
    {
      // Set EE target wp
      this->alg_.lock();
      this->alg_.eepos_des_ = this->in_ee_wp_.block(0,this->curr_ee_wp_idx_,6,1);
      this->alg_.unlock();

      // Time differential
      this->dt_ = curr_t - this->time_last_;

      this->alg_.lock();

      // Set control parameters
      this->alg_.ctrl_params_.enable_sec_task = this->enable_sec_task_;
      this->alg_.ctrl_params_.q_rollpitch = this->q_rollpitch_;
      this->alg_.ctrl_params_.v_rollpitch = this->v_rollpitch_;
      this->alg_.ctrl_params_.jntlim_pos_des = this->q_des_;
      this->alg_.ctrl_params_.dt = this->dt_;
      this->alg_.ctrl_params_.ir_gain = this->gain_sec_task_(0,0);
      this->alg_.ctrl_params_.cog_gain = this->gain_sec_task_(1,0);
      this->alg_.ctrl_params_.jntlim_gain = this->gain_sec_task_(2,0);
      this->alg_.ctrl_params_.vs_alpha_min = this->vs_alpha_min_;
      this->alg_.ctrl_params_.vs_delta_gain = this->vs_delta_gain_;
      this->alg_.ctrl_params_.inf_radius = this->alg_.inf_radius_;

      // Set Homogeneous transformations
      this->alg_.HT_.cam_in_tip = this->Tcam_in_tip_;
      this->alg_.HT_.armbase_in_baselink = this->Tarmbase_in_baselink_;
      this->alg_.HT_.link1_in_armbase = this->Tlink1_in_armbase_;

      // Main algorithm
      this->alg_.const_task_opt(this->obs_inside_ir_sphere_, curr_t, this->current_uam_pos_, this->current_uam_vel_, this->new_uam_pos_, this->debug_data_);

      this->alg_.unlock();

      // Publish debug data
      this->debug_data_msg_.layout.dim.resize(1);
      this->debug_data_msg_.layout.dim[0].label = "";
      this->debug_data_msg_.layout.dim[0].size = this->debug_data_.size();
      this->debug_data_msg_.layout.dim[0].stride = this->debug_data_.size();
      this->debug_data_msg_.layout.data_offset = 0;

      this->debug_data_msg_.data.resize(this->debug_data_.size());
      for (int ii = 0; ii < this->debug_data_.size(); ++ii)
        this->debug_data_msg_.data[ii] = this->debug_data_.at(ii);
      this->debug_data_publisher_.publish(this->debug_data_msg_);

      // Fill marker array
      fill_marker_array();
      this->marker_array_publisher_.publish(this->MarkerArray_msg_);

      // fill quadrotor velocity msg
      fill_quad_wp();
      waypointsMakeActionRequest();

      // Fill arm joints trajectory
      fill_joints_traj();
      this->joint_traj_publisher_.publish(this->joint_traj_msg_);

      // Update time
      this->time_last_ = curr_t;
    }
  }

  // [fill msg structures]

  // [fill srv structure and make request to the server]

  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */
void KintonArmConstrTaskOptAlgNode::joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg)
{
  ROS_DEBUG("KintonArmConstrTaskOptAlgNode::joint_states_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  this->joint_states_mutex_enter();

  for (int ii = 0; ii < msg->name.size(); ++ii)
  {
    std::stringstream jname;
    jname << "joint" << ii+1;
    if (jname.str().compare(msg->name[ii]) == 0)
    {
      this->current_joint_pos_(ii,0) = msg->position[ii];
      this->current_joint_vel_(ii,0) = msg->velocity[ii];
    }
  }

  //unlock previously blocked shared variables
  this->joint_states_mutex_exit();
}

void KintonArmConstrTaskOptAlgNode::joint_states_mutex_enter(void)
{
  pthread_mutex_lock(&this->joint_states_mutex_);
}

void KintonArmConstrTaskOptAlgNode::joint_states_mutex_exit(void)
{
  pthread_mutex_unlock(&this->joint_states_mutex_);
}

void KintonArmConstrTaskOptAlgNode::pose_callback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
  ROS_DEBUG("KintonArmConstrTaskOptAlgNode::pose_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  this->pose_mutex_enter();

  Eigen::MatrixXd old_quad_pos = this->current_quad_pos_;

  this->current_quad_pos_(0,0) = msg->transform.translation.x;
  this->current_quad_pos_(1,0) = msg->transform.translation.y;
  this->current_quad_pos_(2,0) = msg->transform.translation.z;

  double roll, pitch,yaw;
  tf::Quaternion qt;
  tf::quaternionMsgToTF(msg->transform.rotation,qt);
  tf::Matrix3x3(qt).getRPY(roll,pitch,yaw);

  this->current_quad_pos_(3,0) = roll;
  this->current_quad_pos_(4,0) = pitch;
  this->current_quad_pos_(5,0) = yaw;

  // Compute current velocity using old position
  if (this->dt_>0.0)
  {
    this->current_quad_vel_ = (this->current_quad_pos_ - old_quad_pos)/this->dt_;
  }
  else
    this->current_quad_vel_ = Eigen::MatrixXd::Zero(6,1);

  //unlock previously blocked shared variables
  this->pose_mutex_exit();
}

void KintonArmConstrTaskOptAlgNode::pose_mutex_enter(void)
{
  pthread_mutex_lock(&this->pose_mutex_);
}

void KintonArmConstrTaskOptAlgNode::pose_mutex_exit(void)
{
  pthread_mutex_unlock(&this->pose_mutex_);
}

/*  [service callbacks] */

/*  [action callbacks] */
void KintonArmConstrTaskOptAlgNode::in_ee_wpStartCallback(const kinton_msgs::waypointsGoalConstPtr& goal)
{
  this->alg_.lock();

  // Get waypoints
  int num_wp = goal->waypoints.size();

  if (num_wp > 0)
  {
    this->in_ee_wp_ = Eigen::MatrixXd::Zero(this->in_ee_wp_elements_,num_wp);
    for (int wp = 0; wp < num_wp; ++wp)
    {
      this->in_ee_wp_(0,wp) = goal->waypoints[wp].pose.position.x;
      this->in_ee_wp_(1,wp) = goal->waypoints[wp].pose.position.y;
      this->in_ee_wp_(2,wp) = goal->waypoints[wp].pose.position.z;

      // Convert quaternion to RPY.
      tf::Quaternion qt;
      double roll, pitch, yaw;
      tf::quaternionMsgToTF(goal->waypoints[wp].pose.orientation, qt);
      tf::Matrix3x3(qt).getRPY(roll, pitch, yaw);

      this->in_ee_wp_(3,wp) = roll;
      this->in_ee_wp_(4,wp) = pitch;
      this->in_ee_wp_(5,wp) = yaw;
      this->in_ee_wp_(6,wp) = goal->waypoints[wp].max_confidence_error[0];
      this->in_ee_wp_(7,wp) = goal->waypoints[wp].max_confidence_error[1];
      this->in_ee_wp_(8,wp) = goal->waypoints[wp].wait;
    }
    this->in_ee_wp_action_active_ = true;
    this->in_ee_wp_action_succeeded_ = false;
    this->in_ee_wp_action_finished_ = false;

    // reset waypoint iterator
    this->curr_ee_wp_idx_ = 0;
    
    ROS_DEBUG("[kinton_arm_constr_task_opt]: in_ee_wpStartCallback: New waypoint list. Starting control.");
  }
  else
    ROS_WARN("[kinton_arm_constr_task_opt]: in_ee_wpStartCallback: Empty goal received. No waypoints found.");

  //execute goal
  this->alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::in_ee_wpStopCallback(void)
{
  this->alg_.lock();
  //stop action
  this->in_ee_wp_action_active_ = false;
  this->alg_.unlock();
}

bool KintonArmConstrTaskOptAlgNode::in_ee_wpIsFinishedCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if action has finish for any reason
  ret = this->in_ee_wp_action_finished_;
  this->alg_.unlock();

  return ret;
}

bool KintonArmConstrTaskOptAlgNode::in_ee_wpHasSucceededCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if goal was accomplished
  ret = this->in_ee_wp_action_succeeded_;
  this->in_ee_wp_action_active_ = false;
  this->alg_.unlock();

  return ret;
}

void KintonArmConstrTaskOptAlgNode::in_ee_wpGetResultCallback(kinton_msgs::waypointsResultPtr& result)
{
  this->alg_.lock();
  //update result data to be sent to client
  result->current_wp = this->curr_ee_wp_idx_;
  this->alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::in_ee_wpGetFeedbackCallback(kinton_msgs::waypointsFeedbackPtr& feedback)
{
  this->alg_.lock();
  //update feedback data to be sent to client
  feedback->busy = true;
  feedback->target_wp = this->curr_ee_wp_idx_;
  feedback->dist_linear_to_goal = this->curr_ee_dist_error_;
  feedback->dist_angular_to_goal = this->curr_ee_ang_error_;
  this->alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::waypointsDone(const actionlib::SimpleClientGoalState& state,  const kinton_msgs::waypointsResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
    ROS_DEBUG("KintonArmConstrTaskOptAlgNode::waypointsDone: waypointsDone: Goal Achieved!");
  else
    ROS_DEBUG("KintonArmConstrTaskOptAlgNode::waypointsDone: waypointsDone: %s", state.toString().c_str());

  //copy & work with requested result
  alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::waypointsActive()
{
  alg_.lock();
  //ROS_INFO("KintonArmConstrTaskOptAlgNode::waypointsActive: Goal just went active!");
  alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::waypointsFeedback(const kinton_msgs::waypointsFeedbackConstPtr& feedback)
{
  alg_.lock();
  //ROS_INFO("KintonArmConstrTaskOptAlgNode::waypointsFeedback: Got Feedback!");

  bool feedback_is_ok = true;

  //analyze feedback
  //my_var = feedback->var;

  //if feedback is not what expected, cancel requested goal
  if( !feedback_is_ok )
  {
    waypoints_client_.cancelGoal();
    //ROS_INFO("KintonArmConstrTaskOptAlgNode::waypointsFeedback: Cancelling Action!");
  }
  alg_.unlock();
}

bool KintonArmConstrTaskOptAlgNode::ee_inside_confidence_zone(const double& max_e_lin, const double& max_e_ang)
{
  Eigen::VectorXd elin = this->in_ee_wp_.block(0,this->curr_ee_wp_idx_,3,1) - this->alg_.HT_.cam_in_w.block(0,3,3,1);
  Eigen::VectorXd eang = this->in_ee_wp_.block(3,this->curr_ee_wp_idx_,3,1) - RToEulerContinuous(this->alg_.HT_.cam_in_w.block(0,0,3,3),this->in_ee_wp_.block(3,this->curr_ee_wp_idx_,3,1));
  Eigen::VectorXd ee_error_lin = elin.transpose()*elin;
  Eigen::VectorXd ee_error_ang = eang.transpose()*eang;

  this->curr_ee_dist_error_ = sqrt(ee_error_lin(0));
  this->curr_ee_ang_error_ = sqrt(ee_error_ang(0));

  this->is_in_dist_zone_lin_ = false;
  if (this->curr_ee_dist_error_< max_e_lin)
    this->is_in_dist_zone_lin_ = true;

  this->is_in_dist_zone_ang_ = false;
  if (this->curr_ee_ang_error_< max_e_ang)
    this->is_in_dist_zone_ang_ = true;

  bool is_in_dist_zone = false;
  if(this->is_in_dist_zone_lin_ && this->is_in_dist_zone_ang_)
    is_in_dist_zone = true;  

  return is_in_dist_zone;
}

bool KintonArmConstrTaskOptAlgNode::hasWaited(const double& t_wait)
{
  bool waitdone = false;

  ros::Duration dur_inpose = ros::Time::now() - this->time_inpose_;

  if (dur_inpose.toSec() > 0.1 && (dur_inpose.toSec()<0.1+this->dt_))
    ROS_INFO("[kinton_arm_constr_task_opt]: Destination reached. Counting down from: %.2fs",(t_wait));

  if (dur_inpose.toSec() > t_wait)
  {
    waitdone = true;
    ROS_INFO("[kinton_arm_constr_task_opt]: Waiting in destination done.");
  }

  return waitdone;
}

/*  [action requests] */
bool KintonArmConstrTaskOptAlgNode::waypointsMakeActionRequest()
{
  // IMPORTANT: Please note that all mutex used in the client callback functions
  // must be unlocked before calling any of the client class functions from an
  // other thread (MainNodeThread).
  // this->alg_.unlock();
  if(waypoints_client_.isServerConnected())
  {
    Eigen::MatrixXd waypoints = this->out_quad_wp_;

    int num_wp = waypoints.cols();

    waypoints_goal_.waypoints.resize(num_wp);

    for (int wp = 0; wp < num_wp; ++wp)
    {
      waypoints_goal_.waypoints[wp].pose.position.x = waypoints(0,wp);
      waypoints_goal_.waypoints[wp].pose.position.y = waypoints(1,wp);
      waypoints_goal_.waypoints[wp].pose.position.z = waypoints(2,wp);

      tf::Quaternion qt;
      qt.setRPY(0.0,0.0,waypoints(3,wp));
      geometry_msgs::Quaternion qt_msg;
      tf::quaternionTFToMsg(qt,qt_msg);
      waypoints_goal_.waypoints[wp].pose.orientation = qt_msg;

      waypoints_goal_.waypoints[wp].cruise = waypoints(4,wp);
      waypoints_goal_.waypoints[wp].max_confidence_error[0] = waypoints(5,wp);
      waypoints_goal_.waypoints[wp].max_confidence_error[1] = waypoints(6,wp);
      waypoints_goal_.waypoints[wp].wait = waypoints(7,wp);
    }

    ROS_DEBUG("KintonArmConstrTaskOptAlgNode::waypointsMakeActionRequest: Server is Available!");
    //send a goal to the action server
    waypoints_client_.sendGoal(waypoints_goal_,
                boost::bind(&KintonArmConstrTaskOptAlgNode::waypointsDone,     this, _1, _2),
                boost::bind(&KintonArmConstrTaskOptAlgNode::waypointsActive,   this),
                boost::bind(&KintonArmConstrTaskOptAlgNode::waypointsFeedback, this, _1));
    // this->alg_.lock();
    ROS_DEBUG("KintonArmConstrTaskOptAlgNode::MakeActionRequest: Goal Sent.");
    return true;
  }
  else
  {
    // this->alg_.lock();
    ROS_INFO("KintonArmConstrTaskOptAlgNode::waypointsMakeActionRequest: HRI server is not connected");
    return false;
  }
}

/*  Constructor initializations  */
void KintonArmConstrTaskOptAlgNode::read_params(void)
{
  // Initialize all variables
  this->ini_uam_pos_ = Eigen::MatrixXd::Zero(12, 1);
  this->vs_delta_gain_ = Eigen::MatrixXd::Zero(2, 1);
  this->alg_.inf_radius_ = 0.0;
  this->enable_sec_task_ = false;
  this->gain_sec_task_ = Eigen::MatrixXd::Zero(3,1);
  this->Tcam_in_tip_ = Eigen::Matrix4d::Identity();
  this->Tarmbase_in_baselink_ = Eigen::Matrix4d::Identity();

  // Get quadrotor and arm initial values
  this->public_node_handle_.param<double>("quad_x_ini", this->ini_uam_pos_(0, 0), 0);
  this->public_node_handle_.param<double>("quad_y_ini", this->ini_uam_pos_(1, 0), 0);
  this->public_node_handle_.param<double>("quad_z_ini", this->ini_uam_pos_(2, 0),2.5);
  this->public_node_handle_.param<double>("quad_roll_ini", this->ini_uam_pos_(3, 0), 0);
  this->public_node_handle_.param<double>("quad_pitch_ini", this->ini_uam_pos_(4, 0), 0);
  this->public_node_handle_.param<double>("quad_yaw_ini", this->ini_uam_pos_(5, 0), 0);
  this->public_node_handle_.param<double>("arm_q1_ini", this->ini_uam_pos_(6, 0), 0);
  this->public_node_handle_.param<double>("arm_q2_ini", this->ini_uam_pos_(7, 0),0.2);
  this->public_node_handle_.param<double>("arm_q3_ini", this->ini_uam_pos_(8, 0),1.7);
  this->public_node_handle_.param<double>("arm_q4_ini", this->ini_uam_pos_(9, 0), 2);
  this->public_node_handle_.param<double>("arm_q5_ini", this->ini_uam_pos_(10, 0), 0);
  this->public_node_handle_.param<double>("arm_q6_ini", this->ini_uam_pos_(11, 0), 0);

  std::string robot_prefix = "";
  if (!this->public_node_handle_.getParam("robot_prefix", robot_prefix))
    ROS_ERROR("No robot_prefix found on parameter server");

  // Get values for VS inverse gain matrix
  this->public_node_handle_.param<double>("W_alpha_min", this->vs_alpha_min_,-0.1);
  this->public_node_handle_.param<double>("W_min", this->vs_delta_gain_(0, 0),0.2);
  this->public_node_handle_.param<double>("W_max", this->vs_delta_gain_(1, 0),1.5);

  // Get values for security measure (minimum distance to the ground)
  this->public_node_handle_.param<double>("Inf_radius", this->alg_.inf_radius_,0.2);

  // URDF Model information
  ros::NodeHandle nh;

  std::string tip_name, root_name, optical_name;
  std::string xml_string;
  KDL::Tree tree;
  std::string urdf_xml, full_urdf_xml;
  nh.param("urdf_xml", urdf_xml, std::string("robot_description"));
  nh.searchParam(urdf_xml, full_urdf_xml);
  ROS_DEBUG("Reading xml file from parameter server\n");
  if (!nh.getParam(full_urdf_xml, xml_string))
    ROS_ERROR("Could not load the xml from parameter server: %s\n",urdf_xml.c_str());
  if (!nh.getParam("root_name", root_name))
    ROS_ERROR("No root name found on parameter server");
  if (!nh.getParam("tip_name", tip_name))
    ROS_ERROR("No tip name found on parameter server");

  // KDL Parser to create a KDL chain
  if (!kdl_parser::treeFromString(xml_string, tree))
    ROS_ERROR("Could not initialize tree object");
  if (!tree.getChain(root_name, tip_name, this->alg_.arm_.chain))
    ROS_ERROR("Could not initialize kdl_chain object");

  // Get Joints Information
  urdf::Model robot_model;
  if (!robot_model.initString(xml_string))
    ROS_ERROR("Could not initialize robot model");

  if (!read_joints(robot_model, root_name, tip_name))
    ROS_ERROR("Could not read information about the joints");
  if (!nh.getParam("optical_frame_id", optical_name))
    ROS_ERROR("No camera optical frame name found on parameter server");

  std::stringstream tf_tip,tf_optical;
  tf_tip << robot_prefix << "/" << tip_name;
  tf_optical << robot_prefix << "/" << optical_name;

  // Vars depending on number of joints
  this->public_node_handle_.param<bool>("enable_sec_task", this->enable_sec_task_, true);
  this->public_node_handle_.param<double>("gain_sec_task_IR", this->gain_sec_task_(0,0) , 1.0);
  this->public_node_handle_.param<double>("gain_sec_task_CoG", this->gain_sec_task_(1,0) , 1.0);
  this->public_node_handle_.param<double>("gain_sec_task_jntlim", this->gain_sec_task_(2,0) , 1.0);

  // Get Homogenous transform
  this->Tcam_in_tip_ = get_HT_from_tf(tf_optical.str(), tf_tip.str());
  this->Tarmbase_in_baselink_ =  get_HT_from_tf("/kinton/arm_base","/kinton/base_link");
  this->Tlink1_in_armbase_ =  get_HT_from_tf("/kinton/link1","/kinton/arm_base");

  // Optimization parameters
  this->public_node_handle_.param<double>("l1", this->alg_.params_.l1, 0.2);
  this->public_node_handle_.param<double>("l2", this->alg_.params_.l2, 1.0);
  this->public_node_handle_.param<double>("Ts_horizon", this->alg_.params_.tstep_horizon, 1/5.0);
  this->public_node_handle_.param<double>("tau_der", this->alg_.params_.tau_der, 0.02);
  this->public_node_handle_.param<int>("nWSR", this->alg_.params_.nWSR, 10);
  this->public_node_handle_.param<double>("time_cpu_max", this->alg_.params_.time_cpu_max, 0.5);
  this->alg_.params_.num_vars = 4+this->alg_.arm_.nj;
}

bool KintonArmConstrTaskOptAlgNode::read_joints(urdf::Model &robot_model, const std::string& root_name, const std::string& tip_name)
{
  int num_joints = 0;

  // get joint maxs and mins
  boost::shared_ptr<const urdf::Link> link = robot_model.getLink(tip_name);
  boost::shared_ptr<const urdf::Joint> joint;

  while (link && link->name != root_name)
  {
    joint = robot_model.getJoint(link->parent_joint->name);
    if (!joint)
    {
      ROS_ERROR("Could not find joint: %s", link->parent_joint->name.c_str());
      return false;
    }
    if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
    {
      ROS_DEBUG( "read_joints: adding joint: [%s]", joint->name.c_str() );
      num_joints++;
    }
    link = robot_model.getLink(link->getParent()->name);
  }

  this->alg_.arm_.nj = num_joints;
  this->alg_.arm_.joint_info.resize(num_joints);

  link = robot_model.getLink(tip_name);
  int ii = 0;
  while (link && ii < num_joints)
  {
    joint = robot_model.getJoint(link->parent_joint->name);
    if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
    {
      ROS_DEBUG( "read_joints: getting bounds for joint: [%s]", joint->name.c_str() );
      float lower, upper;
      if (joint->type != urdf::Joint::CONTINUOUS)
      {
        lower = joint->limits->lower;
        upper = joint->limits->upper;
      }
      else
      {
        lower = -M_PI;
        upper = M_PI;
      }
      int index = num_joints - ii - 1;
      this->alg_.arm_.joint_info.at(index).joint_min = lower;
      this->alg_.arm_.joint_info.at(index).joint_max = upper;
      this->alg_.arm_.joint_info.at(index).link_child = link->name;
      this->alg_.arm_.joint_info.at(index).link_parent = link->getParent()->name;
      ii++;
    }
    link = robot_model.getLink(link->getParent()->name);
  }
  return true;
}

Eigen::Matrix4d KintonArmConstrTaskOptAlgNode::get_HT_from_tf(const std::string& cam_frame, const std::string& tip_frame)
{
  Eigen::Matrix4d Tcam_in_tip = Eigen::Matrix4d::Identity();

  bool transform_ok = false;

  tf::TransformListener listener;
  tf::StampedTransform transform;

  while (!transform_ok)
  {
    try
    {
      listener.waitForTransform(tip_frame, cam_frame, ros::Time(0), ros::Duration(1.0));
      listener.lookupTransform(tip_frame, cam_frame, ros::Time(0), transform);
      transform_ok = true;
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("[kinton_arm_constr_task_opt_alg_node]: Kinton Transform error: %s", ex.what());
    }
  }
  // tf transform to Eigen homogenous matrix
  bool T_ok = tf_to_eigen(transform,Tcam_in_tip);
  if (!T_ok)
    ROS_ERROR("get_HT_from_tf: Wrong tf to Eigen transform");

  return Tcam_in_tip;
}

bool KintonArmConstrTaskOptAlgNode::tf_to_eigen(const tf::Transform& transform, Eigen::Matrix4d& eigenT)
{
  double roll, pitch, yaw;
  transform.getBasis().getRPY(roll, pitch, yaw);

  // Euler ZYX convention
  Eigen::Matrix3d Rot;
  Rot = Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ())
      * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY())
      * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX());

  bool T_ok = false;

  eigenT = Eigen::Matrix4d::Identity();
  if (!std::isnan(Rot(0, 0)))
  {
    eigenT.block(0, 0, 3, 3) = Rot;
    eigenT(0, 3) = transform.getOrigin().x();
    eigenT(1, 3) = transform.getOrigin().y();
    eigenT(2, 3) = transform.getOrigin().z();
    T_ok = true;
  }

  return T_ok;
}

void KintonArmConstrTaskOptAlgNode::eigen_to_tf(const  Eigen::Matrix4d& eigenT, tf::Transform& transform)
{
  double x,y,z,roll,pitch,yaw;
  x = eigenT(0,3);
  y = eigenT(1,3);
  z = eigenT(2,3);
  Eigen::Matrix3d Rot = eigenT.block(0,0,3,3);
  Eigen::Vector3d angles = Rot.eulerAngles(2, 1, 0);

  transform.setOrigin( tf::Vector3(x, y, z) );
  tf::Quaternion q;
  q.setRPY(angles(2,0),angles(1,0),angles(0,0));
  transform.setRotation(q);
}

Eigen::Vector3d KintonArmConstrTaskOptAlgNode::RToEulerContinuous(const Eigen::Matrix3d& Rot, const Eigen::Vector3d& rpy_old)
{
  Eigen::Vector3d ang;

  if (Rot(2,0)!=-1.0 && Rot(2,0)!=1.0)
  {
    double phi1, theta1, psi1;
    double phi2, theta2, psi2;

    Eigen::Vector3d ang1;
    Eigen::Vector3d ang2;

    theta1 = -asin(Rot(2,0));
    theta2 = 3.141592653589793 - theta1;
    psi1 = atan2(Rot(2,1)/cos(theta1),Rot(2,2)/cos(theta1)); 
    psi2 = atan2(Rot(2,1)/cos(theta2),Rot(2,2)/cos(theta2)); 
    phi1 = atan2(Rot(1,0)/cos(theta1),Rot(0,0)/cos(theta1)); 
    phi2 = atan2(Rot(1,0)/cos(theta2),Rot(0,0)/cos(theta2)); 

    ang1 << psi1, theta1, phi1;
    ang2 << psi2, theta2, phi2;

    if ( (rpy_old-ang1).norm() < (rpy_old-ang2).norm())
      ang = ang1;
    else
      ang = ang2; // ZYX Euler convention.
  }
  else
  {
    double phi, theta, psi;
    phi = 0.0;
    if (Rot(2,0)==-1.0)
    {
      theta = 3.141592653589793/2;
      psi = phi + atan2(Rot(0,1),Rot(0,2)); 
    }
    else
    {
      theta = -3.141592653589793/2;
      psi = -phi + atan2(-Rot(0,1),-Rot(0,2)); 
    }
    ang << phi, theta, psi;
  }
  return ang;
}


void KintonArmConstrTaskOptAlgNode::broadcast_tf(const  Eigen::Matrix4d& eigenT, const std::string& parent, const std::string& child)
{
  tf::Transform transform;
  eigen_to_tf(eigenT,transform);
  broadcast_tf(transform,parent,child);
}

void KintonArmConstrTaskOptAlgNode::broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child)
{
  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent, child));
}

void KintonArmConstrTaskOptAlgNode::init_arm(void)
{
  this->new_uam_pos_ = Eigen::MatrixXd::Zero(6 + this->alg_.arm_.nj, 1);
  this->new_uam_pos_.block(0, 0, 6, 1) = this->ini_uam_pos_.block(0, 0, 6, 1);
  this->q_des_ = Eigen::MatrixXd::Zero(this->alg_.arm_.nj, 1);
  this->q_des_ << 0.0, 0.0, 0.785, 1.57, 0.785, 0.0;
  this->new_uam_pos_.block(6, 0, this->alg_.arm_.nj, 1) << 0.0, 0.0, 0.0, 1.745, 1.0, 0.0;
}

void KintonArmConstrTaskOptAlgNode::sim_cyl_collision(void)
{
  // Simulate big cylinder:
  // position in world
  this->alg_.obs_in_w_(0,0) = 0.0;
  this->alg_.obs_in_w_(1,0) = -1.0;
  this->alg_.obs_in_w_(2,0) = 1.0;
  // size
  this->alg_.obs_in_w_(0,1) = 0.4;
  this->alg_.obs_in_w_(1,1) = 0.4;
  this->alg_.obs_in_w_(2,1) = 2.0;
}

void KintonArmConstrTaskOptAlgNode::sim_legs_collision(void)
{
  // Two points for each leg (xyz) in baselink
  // Leg 1
  this->alg_.legpoints_in_baselink_(0,0) = 0.07;
  this->alg_.legpoints_in_baselink_(1,0) = 0.07;
  this->alg_.legpoints_in_baselink_(2,0) = -0.07;
  this->alg_.legpoints_in_baselink_(3,0) = 1.0;
  this->alg_.legpoints_in_baselink_(0,1) = 0.14;
  this->alg_.legpoints_in_baselink_(1,1) = 0.14;
  this->alg_.legpoints_in_baselink_(2,1) = -0.25;
  this->alg_.legpoints_in_baselink_(3,1) = 1.0;

  // Leg 2
  this->alg_.legpoints_in_baselink_(0,2) = -0.07;
  this->alg_.legpoints_in_baselink_(1,2) = 0.07;
  this->alg_.legpoints_in_baselink_(2,2) = -0.07;
  this->alg_.legpoints_in_baselink_(3,2) = 1.0;
  this->alg_.legpoints_in_baselink_(0,3) = -0.14;
  this->alg_.legpoints_in_baselink_(1,3) = 0.14;
  this->alg_.legpoints_in_baselink_(2,3) = -0.25;  
  this->alg_.legpoints_in_baselink_(3,3) = 1.0;

  // Leg 3
  this->alg_.legpoints_in_baselink_(0,4) = -0.07;
  this->alg_.legpoints_in_baselink_(1,4) = -0.07;
  this->alg_.legpoints_in_baselink_(2,4) = -0.07;
  this->alg_.legpoints_in_baselink_(3,4) = 1.0;
  this->alg_.legpoints_in_baselink_(0,5) = -0.14;
  this->alg_.legpoints_in_baselink_(1,5) = -0.14;
  this->alg_.legpoints_in_baselink_(2,5) = -0.25;  
  this->alg_.legpoints_in_baselink_(3,5) = 1.0;

  // Leg 4
  this->alg_.legpoints_in_baselink_(0,6) = 0.07;
  this->alg_.legpoints_in_baselink_(1,6) = -0.07;
  this->alg_.legpoints_in_baselink_(2,6) = -0.07;
  this->alg_.legpoints_in_baselink_(3,6) = 1.0;
  this->alg_.legpoints_in_baselink_(0,7) = 0.14;
  this->alg_.legpoints_in_baselink_(1,7) = -0.14;
  this->alg_.legpoints_in_baselink_(2,7) = -0.25;  
  this->alg_.legpoints_in_baselink_(3,7) = 1.0;

  for (int ii = 0; ii < this->alg_.legpoints_in_baselink_.cols(); ++ii)
    this->alg_.legpoints_in_armbase_.col(ii) = this->Tarmbase_in_baselink_.inverse()*this->alg_.legpoints_in_baselink_.col(ii);
}

/*  Fill publishers  */
void KintonArmConstrTaskOptAlgNode::fill_marker_array()
{
  this->MarkerArray_msg_.markers.clear();

  ros::Time stamp = ros::Time::now();

  // ARM COG
  visualization_msgs::Marker cog_marker;
  cog_marker.header.frame_id = "/kinton/base_link";
  cog_marker.header.stamp = stamp;
  cog_marker.ns = "cog";
  cog_marker.type = visualization_msgs::Marker::SPHERE;
  cog_marker.action = visualization_msgs::Marker::ADD;
  cog_marker.lifetime = ros::Duration();
  cog_marker.id = 0;
 
  double cog_x = this->alg_.arm_cog_data_.arm_cog(0,0);
  double cog_y = this->alg_.arm_cog_data_.arm_cog(1,0);
  double cog_z = this->alg_.arm_cog_data_.arm_cog(2,0);
 
  cog_marker.pose.position.x = cog_x;
  cog_marker.pose.position.y = cog_y;
  cog_marker.pose.position.z = cog_z;
 
  cog_marker.pose.orientation.x = 0.0;
  cog_marker.pose.orientation.y = 0.0;
  cog_marker.pose.orientation.z = 0.0;
  cog_marker.pose.orientation.w = 1.0;
 
  cog_marker.scale.x = 0.025;
  cog_marker.scale.y = 0.025;
  cog_marker.scale.z = 0.025;
 
  double xy_dist = std::sqrt(std::pow(cog_x,2) + std::pow(cog_y,2));
  double e_radius = 0.02;
 
  if (xy_dist > e_radius)
  {
    cog_marker.color.r = 1.0;
    cog_marker.color.g = 0.0;
    cog_marker.color.b = 0.0;
  }
  else
  {
    cog_marker.color.r = xy_dist/e_radius;
    cog_marker.color.g = 1-(xy_dist/e_radius);
    cog_marker.color.b = 0.0;
  }
 
  cog_marker.color.a = 1.0;
 
  this->MarkerArray_msg_.markers.push_back(cog_marker);
 
  // Link COG
  for (int ii = 0; ii < this->alg_.arm_cog_data_.link_cog.size(); ++ii)
  {
    visualization_msgs::Marker cog_link_marker;
    cog_link_marker.header.frame_id = "/kinton/base_link";
    cog_link_marker.header.stamp = stamp;
    cog_link_marker.ns = "cog";
    cog_link_marker.type = visualization_msgs::Marker::SPHERE;
    cog_link_marker.action = visualization_msgs::Marker::ADD;
    cog_link_marker.lifetime = ros::Duration();
    cog_link_marker.id = ii+1;
 
    cog_link_marker.pose.position.x = this->alg_.arm_cog_data_.link_cog.at(ii)(0,0);
    cog_link_marker.pose.position.y = this->alg_.arm_cog_data_.link_cog.at(ii)(1,0);
    cog_link_marker.pose.position.z = this->alg_.arm_cog_data_.link_cog.at(ii)(2,0);
 
    cog_link_marker.pose.orientation.x = 0.0;
    cog_link_marker.pose.orientation.y = 0.0;
    cog_link_marker.pose.orientation.z = 0.0;
    cog_link_marker.pose.orientation.w = 1.0;
 
    cog_link_marker.scale.x = 0.01;
    cog_link_marker.scale.y = 0.01;
    cog_link_marker.scale.z = 0.01;
    cog_link_marker.color.r = 1.0f;
    cog_link_marker.color.g = 0.4f;
    cog_link_marker.color.b = 0.0f;
    cog_link_marker.color.a = 1.0;
 
    this->MarkerArray_msg_.markers.push_back(cog_link_marker);
  }

  if (this->in_ee_wp_action_active_)
  {
    // EE Trajectory
    visualization_msgs::Marker ee_wp_marker;

    // EE Trajectory line
    ee_wp_marker = fill_marker_ee_traj();
    this->MarkerArray_msg_.markers.push_back(ee_wp_marker);

    // EE Waypoint
    ee_wp_marker = fill_ee_marker_waypoint(this->MarkerArray_msg_.markers.size()+1);
    this->MarkerArray_msg_.markers.push_back(ee_wp_marker);

    // EE waypoint error text
    ee_wp_marker = fill_ee_marker_text_lin(this->MarkerArray_msg_.markers.size()+1);
    this->MarkerArray_msg_.markers.push_back(ee_wp_marker);
    ee_wp_marker = fill_ee_marker_text_ang(this->MarkerArray_msg_.markers.size()+1);
    this->MarkerArray_msg_.markers.push_back(ee_wp_marker);   

    // EE waypoint tf
    Eigen::Matrix4d HT_eepos_des = Eigen::Matrix4d::Identity();
    Eigen::Matrix3d Rot;
    Rot = Eigen::AngleAxisd(this->alg_.eepos_des_(5,0), Eigen::Vector3d::UnitZ())
        * Eigen::AngleAxisd(this->alg_.eepos_des_(4,0), Eigen::Vector3d::UnitY())
        * Eigen::AngleAxisd(this->alg_.eepos_des_(3,0), Eigen::Vector3d::UnitX());
    HT_eepos_des.block(0,0,3,3) = Rot;
    HT_eepos_des.block(0,3,3,1) = this->alg_.eepos_des_.block(0,0,3,1);
    broadcast_tf(HT_eepos_des,"/optitrak","ee_wp");
  }

  // Inflation Radius
  visualization_msgs::Marker ir_marker;
  ir_marker.header.frame_id = "/kinton/base_link";
  ir_marker.header.stamp = stamp;
  ir_marker.ns = "inflation_radius";
  ir_marker.type = visualization_msgs::Marker::SPHERE;
  ir_marker.action = visualization_msgs::Marker::ADD;
  ir_marker.lifetime = ros::Duration();
  ir_marker.id = this->MarkerArray_msg_.markers.size()+1;

  ir_marker.pose.position.x = 0.0;
  ir_marker.pose.position.y = 0.0;
  ir_marker.pose.position.z = 0.0;

  ir_marker.pose.orientation.x = 0.0;
  ir_marker.pose.orientation.y = 0.0;
  ir_marker.pose.orientation.z = 0.0;
  ir_marker.pose.orientation.w = 1.0;

  ir_marker.scale.x = 2*this->alg_.inf_radius_;
  ir_marker.scale.y = 2*this->alg_.inf_radius_;
  ir_marker.scale.z = 2*this->alg_.inf_radius_;

  ir_marker.color.r = 0.0f;
  ir_marker.color.g = 1.0f;
  ir_marker.color.b = 0.0f;
  if (this->alg_.coll_det_)
  {
    ir_marker.color.r = 1.0f;
    ir_marker.color.g = 0.0f;
  }
  ir_marker.color.a = 0.1;

  this->MarkerArray_msg_.markers.push_back(ir_marker);  

  // Obstacle
  if (this->alg_.sim_cyl_collision_)
  {
    visualization_msgs::Marker obstacle_marker;
    obstacle_marker.header.frame_id = "/optitrak";
    obstacle_marker.header.stamp = stamp;
    obstacle_marker.ns = "obstacle";
    obstacle_marker.type = visualization_msgs::Marker::CYLINDER;
    obstacle_marker.action = visualization_msgs::Marker::ADD;
    obstacle_marker.lifetime = ros::Duration();
    obstacle_marker.id = this->MarkerArray_msg_.markers.size()+1;

    obstacle_marker.pose.position.x = this->alg_.obs_in_w_(0,0);
    obstacle_marker.pose.position.y = this->alg_.obs_in_w_(1,0);
    obstacle_marker.pose.position.z = this->alg_.obs_in_w_(2,0);

    obstacle_marker.pose.orientation.x = 0.0;
    obstacle_marker.pose.orientation.y = 0.0;
    obstacle_marker.pose.orientation.z = 0.0;
    obstacle_marker.pose.orientation.w = 1.0;

    obstacle_marker.scale.x = this->alg_.obs_in_w_(0,1);
    obstacle_marker.scale.y = this->alg_.obs_in_w_(1,1);
    obstacle_marker.scale.z = this->alg_.obs_in_w_(2,1);
    obstacle_marker.color.r = 1.0f;
    obstacle_marker.color.g = 0.0f;
    obstacle_marker.color.b = 0.0f;
    obstacle_marker.color.a = 1.0;

    this->MarkerArray_msg_.markers.push_back(obstacle_marker);
  }

  // Obstacle
  if (this->alg_.sim_legs_collision_)
  {
    visualization_msgs::Marker leg_marker;
    for (int ii = 0; ii < 4; ++ii)
    {
      leg_marker = fill_leg_marker(this->MarkerArray_msg_.markers.size()+1,ii,this->alg_.legpoints_in_baselink_);
      this->MarkerArray_msg_.markers.push_back(leg_marker);
    }
  }

}
visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_ee_marker_waypoint(const int& ii)
{
  // waypoints_
  visualization_msgs::Marker current_marker;

  current_marker.header.frame_id = "/optitrak";
  current_marker.header.stamp = ros::Time::now();
  current_marker.ns = "kinton_ee_waypoints";
  current_marker.type = visualization_msgs::Marker::SPHERE;
  current_marker.action = visualization_msgs::Marker::ADD;
  //current_marker.lifetime = ros::Duration(0.02);
  current_marker.id = ii;
  current_marker.pose.position.x = this->in_ee_wp_(0,this->curr_ee_wp_idx_);
  current_marker.pose.position.y = this->in_ee_wp_(1,this->curr_ee_wp_idx_);
  current_marker.pose.position.z = this->in_ee_wp_(2,this->curr_ee_wp_idx_);
  tf::Quaternion qt;
  qt.setRPY(0.0,0.0,0.0);
  geometry_msgs::Quaternion qt_msg;
  tf::quaternionTFToMsg(qt,qt_msg);
  current_marker.pose.orientation = qt_msg;
  current_marker.scale.x = 0.1;
  current_marker.scale.y = 0.1;
  current_marker.scale.z = 0.1;
  current_marker.color.a = 0.2;
  current_marker.color.r = 1.0f;
  current_marker.color.g = 1.0f;
  current_marker.color.b = 0.0f;

  return current_marker;
}
visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_marker_ee_traj(void)
{
  visualization_msgs::Marker current_marker;

  current_marker.header.frame_id = "/optitrak";
  current_marker.header.stamp = ros::Time::now();
  current_marker.ns = "kinton_ee_trajectory";
  current_marker.action = visualization_msgs::Marker::ADD;
  current_marker.pose.orientation.w = 1.0;
  current_marker.id = 1;
  current_marker.type = visualization_msgs::Marker::LINE_STRIP;
  current_marker.scale.x = 0.01;
  current_marker.color.r = 0.0f;
  current_marker.color.g = 1.0f;
  current_marker.color.b = 1.0f;
  current_marker.color.a = 0.5;

  current_marker.points.clear();

  geometry_msgs::Point p;
  p.x = this->alg_.HT_.cam_in_w(0,3);
  p.y = this->alg_.HT_.cam_in_w(1,3);
  p.z = this->alg_.HT_.cam_in_w(2,3);
  current_marker.points.push_back(p);

  p.x = this->in_ee_wp_(0,this->curr_ee_wp_idx_);
  p.y = this->in_ee_wp_(1,this->curr_ee_wp_idx_);
  p.z = this->in_ee_wp_(2,this->curr_ee_wp_idx_);
  current_marker.points.push_back(p);

  return current_marker;
}
visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_ee_marker_text_common(const int& ii)
{
  // waypoints_
  visualization_msgs::Marker current_marker;

  current_marker.header.frame_id = "/optitrak";
  current_marker.header.stamp = ros::Time::now();
  current_marker.ns = "kinton_ee_waypoints";
  current_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  current_marker.action = visualization_msgs::Marker::ADD;
  //current_marker.lifetime = ros::Duration(0.02);
  current_marker.id = ii;
  current_marker.pose.position.x = this->in_ee_wp_(0,this->curr_ee_wp_idx_) + 0.75;
  current_marker.pose.position.y = this->in_ee_wp_(1,this->curr_ee_wp_idx_);
  current_marker.pose.position.z = this->in_ee_wp_(2,this->curr_ee_wp_idx_);
  tf::Quaternion qt;
  qt.setRPY(0.0,0.0,0.0);
  geometry_msgs::Quaternion qt_msg;
  tf::quaternionTFToMsg(qt,qt_msg);
  current_marker.pose.orientation = qt_msg;
  current_marker.scale.x = 0.0;
  current_marker.scale.y = 0.0;
  current_marker.scale.z = 0.2;
  current_marker.color.a = 1.0;
  current_marker.color.b = 0.0;

return current_marker;
}

visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_ee_marker_text_lin(const int& ii)
{

  visualization_msgs::Marker current_marker = fill_ee_marker_text_common(ii);

  if (this->is_in_dist_zone_lin_)
  {
    current_marker.color.r = 0.0;
    current_marker.color.g = 1.0;
  }
  else 
  {
    current_marker.color.r = 1.0;
    current_marker.color.g = 0.0;
  }
  std::ostringstream dist_strs;
  dist_strs.precision(3);
  dist_strs << "Lin:";
  dist_strs << this->curr_ee_dist_error_;
  std::string dist_text = dist_strs.str();
  current_marker.text = dist_text;

  return current_marker;
}

visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_ee_marker_text_ang(const int& ii)
{

  visualization_msgs::Marker current_marker = fill_ee_marker_text_common(ii);

  if (this->is_in_dist_zone_ang_)
  {
    current_marker.color.r = 0.0;
    current_marker.color.g = 1.0;
  }
  else 
  {
    current_marker.color.r = 1.0;
    current_marker.color.g = 0.0;
  }
  std::ostringstream dist_strs;
  dist_strs.precision(3);
  dist_strs << "\n\nAng:";
  dist_strs << this->curr_ee_ang_error_;
  std::string dist_text = dist_strs.str();
  current_marker.text = dist_text;

  return current_marker;
}

visualization_msgs::Marker KintonArmConstrTaskOptAlgNode::fill_leg_marker(const int& ii, const int& leg_num, const Eigen::MatrixXd& points)
{
  visualization_msgs::Marker leg_marker;
  leg_marker.header.frame_id = "/kinton/base_link";
  leg_marker.header.stamp = ros::Time::now();
  leg_marker.ns = "legs";
  leg_marker.type = visualization_msgs::Marker::CYLINDER;
  leg_marker.action = visualization_msgs::Marker::ADD;
  leg_marker.id = ii;

  leg_marker.pose.position.x = points(0,2*leg_num) + (points(0,2*leg_num+1) - points(0,2*leg_num))/2.0;
  leg_marker.pose.position.y = points(1,2*leg_num) + (points(1,2*leg_num+1) - points(1,2*leg_num))/2.0;
  leg_marker.pose.position.z = points(2,2*leg_num) + (points(2,2*leg_num+1) - points(2,2*leg_num))/2.0;

  tf::Quaternion qt;

  if (leg_num == 0)
    qt.setRPY(0.384,-0.384,0.0);
  else if (leg_num == 1)
    qt.setRPY(0.384,0.384,0.0);   
  else if (leg_num == 2)
    qt.setRPY(-0.384,0.384,0.0);   
  else if (leg_num == 3)
    qt.setRPY(-0.384,-0.384,0.0);   

  geometry_msgs::Quaternion qt_msg;
  tf::quaternionTFToMsg(qt,qt_msg);
  leg_marker.pose.orientation = qt_msg;

  leg_marker.scale.x = 2.0*this->alg_.leg_inf_radius_;
  leg_marker.scale.y = 2.0*this->alg_.leg_inf_radius_;
  leg_marker.scale.z = points(2,2*leg_num+1) - points(2,2*leg_num);
  leg_marker.color.r = 0.0f;
  leg_marker.color.g = 1.0f;
  leg_marker.color.b = 0.0f;
  leg_marker.color.a = 0.3;

  if (this->alg_.leg_collision_.at(leg_num))
  {
    leg_marker.color.r = 1.0f;
    leg_marker.color.g = 0.0f;
    leg_marker.color.a = 0.5;
  }

  return leg_marker;
}

void KintonArmConstrTaskOptAlgNode::fill_quad_wp()
{
  this->out_quad_wp_(0,0) = this->new_uam_pos_(0,0);
  this->out_quad_wp_(1,0) = this->new_uam_pos_(1,0);
  this->out_quad_wp_(2,0) = this->new_uam_pos_(2,0);
  this->out_quad_wp_(3,0) = this->new_uam_pos_(5,0);
  this->out_quad_wp_(4,0) = 0.5;
  this->out_quad_wp_(5,0) = 0.01;
  this->out_quad_wp_(6,0) = 0.01;
  this->out_quad_wp_(7,0) = 0.0;
}
void KintonArmConstrTaskOptAlgNode::fill_joints_traj()
{
  this->joint_traj_msg_.header.stamp = ros::Time::now();
  this->joint_traj_msg_.header.frame_id = "arm";
  this->joint_traj_msg_.joint_names.resize(this->alg_.arm_.nj);
  this->joint_traj_msg_.points.resize(1);
  this->joint_traj_msg_.points[0].positions.resize(this->alg_.arm_.nj);
  this->joint_traj_msg_.points[0].velocities.resize(this->alg_.arm_.nj);
  this->joint_traj_msg_.points[0].accelerations.resize(this->alg_.arm_.nj);
  this->joint_traj_msg_.points[0].effort.resize(this->alg_.arm_.nj);
  this->joint_traj_msg_.points[0].time_from_start = ros::Duration(1.0);

  for (int jj = 0; jj < this->alg_.arm_.nj; ++jj)
  {
    std::stringstream name;
    name << "joint" << jj + 1;
    this->joint_traj_msg_.joint_names[jj] = name.str();
    this->joint_traj_msg_.points[0].positions[jj] = this->new_uam_pos_(6 + jj, 0);
    this->joint_traj_msg_.points[0].velocities[jj] = 0.0;
    this->joint_traj_msg_.points[0].accelerations[jj] = 0.0;
    this->joint_traj_msg_.points[0].effort[jj] = 1.0;
  }
}


void KintonArmConstrTaskOptAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void KintonArmConstrTaskOptAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<KintonArmConstrTaskOptAlgNode>(argc, argv, "kinton_arm_constr_task_opt_alg_node");
}
