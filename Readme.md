# ROS Kinton-arm Constrained Task Optimization (Trajectory generation)

ROS wrapper to work with [constr_task_opt](https://gitlab.com/asantamarianavarro/code/task_control/quadratic_programming/constr_task_opt) library. 

**Related publications:** 

[Trajectory Generation for Unmanned Aerial Manipulators through Quadratic Programming](http://www.angelsantamaria.eu/publications/ral17)

R. Rossi, A. Santamaria-Navarro, J. Andrade-Cetto and P. Rocco

IEEE Robotics and Automation Letters, vol. 2, no. 2, pp. 389-396, Apr. 2017 


### Software dependencies

##### C++ constr_task_opt library and dependencies

- [Boost](http://www.boost.org/) (already installed in most Ubuntu LTS)
  
- [Eigen 3](eigen.tuxfamily.org)

- [QPoases](https://projects.coin-or.org/qpOASES)

- [constr_task_opt](https://gitlab.com/asantamarianavarro/code/task_control/quadratic_programming/constr_task_opt)

  - Download the library: `git clone https://gitlab.com/asantamarianavarro/code/task_control/quadratic_programming/constr_task_opt.git`
  - Compile: `cd constr_task_opt && mkdir build && cd build && cmake .. && make` 
  
- [uam_task_ctrl](https://gitlab.com/asantamarianavarro/code/task_control/least_squares/uam_task_ctrl)

  - Download the library: `git clone https://gitlab.iri.upc.edu/asantamaria/uam_task_ctrl.git`
  - Compile: `cd uam_task_ctrl && mkdir build && cd build && cmake .. && make` 

- [kinton_arm](https://devel.iri.upc.edu/pub/labrobotica/drivers/kinton_arm/trunk)
  
  - Download the library: `svn co https://devel.iri.upc.edu/pub/labrobotica/drivers/kinton_arm/trunk kinton_arm`
  - Compile: `cd kinton_arm && mkdir build && cd build && cmake .. && make` 

##### ROS dependencies

- [tf](http://wiki.ros.org/tf), [std_msgs](http://wiki.ros.org/std_msgs), 
[sensor_msgs](http://wiki.ros.org/sensor_msgs), 
[trajectory_msgs](http://wiki.ros.org/trajectory_msgs), 
[visualization_msgs](http://wiki.ros.org/visualization_msgs), 
[geometry_msgs](http://wiki.ros.org/geometry_msgs), 
[kdl_parser](http://wiki.ros.org/kdl_parser), 
[eigen_conversions](http://wiki.ros.org/eigen_conversions),
[actionlib](http://wiki.ros.org/eigen_conversions)

- [iri_base_algorithm](https://gitlab.iri.upc.edu/labrobotica/ros/iri_core/iri_base_algorithm) - IRI ROS metapackage

  - Move to workspace: `roscd && cd ../src`
  - Download the library: `git clone https://gitlab.iri.upc.edu/labrobotica/ros/iri_core/iri_base_algorithm.git`
  - Compile: `roscd && cd .. && catkin_make` 

- [kinton_msgs](https://devel.iri.upc.edu/labrobotica/ros/iri-ros-pkg_hydro/metapackages/kinton_robot/kinton_msgs) - IRI ROS Kinton msgs

  - Move to workspace: `roscd && cd ../src`
  - Download the library: `svn co https://devel.iri.upc.edu/labrobotica/ros/iri-ros-pkg_hydro/metapackages/kinton_robot/kinton_msgs`
  - Compile: `roscd && cd .. && catkin_make` 
  
### Installation

- Move to workspace: `roscd && cd ../src`
- Clone the repository: `git clone https://gitlab.iri.upc.edu/asantamaria/kinton_arm_constr_task_opt.git`
- Compile:  `roscd && cd .. && catkin_make -DCMAKE_BUILD_TYPE=Release`

### Example of usage

- Run `roslaunch kinton_arm_constr_task_opt kinton_arm_constr_task_opt.launch`.

### Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)